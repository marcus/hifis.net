---
date: 2020-10-15
title: Tasks in Oct 2020
service: cloud
---

## Kickoff for the Integration of the initial Services
Having finished the list of the initial service portfolio, the integration of the services will start. 
At first, the exact details of each service and its tasks for service integration will be examined. This goes hand in hand with the first integration steps. It will also result in a specific roadmap to be established by the end of the year.
