---
date: 2022-06-30
title: Tasks in June 2022
service: software
---

## Publish training program for the second half of 2022

The education and training work package publishes the offered training events for the second half of 2022 on [events.hifis.net](https://events.hifis.net/category/4/).
