#/bin/bash

# This script is to download and place correctly materials from external (still HIFIS maintained) ressources.
# So far, this is done one-by-one, might be automated or looped some time in the future, when series of alike data shall be integrated

# A little helper function to include the contents of an external site generated with mkdocs.
# Parameter 1 is the site to include,
# Parameter 2 is the directory under which to place the included site
function include_mkdocs(){
  pushd $2
    wget $1 -O download.zip
    unzip download.zip
    # Cleanup
    rm -v download.zip
    mv -v public/* .
    rm -v 404.*  # We have our own, thank you very much
  popd
}

# ----------

# 1. Get content for education/training materials.
include_mkdocs \
  "https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-oop-in-python/-/jobs/artifacts/main/download?job=pages" \
  "workshop-materials/python-oop/"

include_mkdocs \
  "https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-pandas/-/jobs/artifacts/main/download?job=pages" \
  "workshop-materials/python-pandas/"

include_mkdocs \
  "https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-matplotlib/-/jobs/artifacts/main/download?job=pages" \
  "workshop-materials/python-matplotlib/"

include_mkdocs \
  "https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/gitlab-ci/workshop-materials/-/jobs/artifacts/main/download?job=pages" \
  "workshop-materials/gitlab-ci/"

# 2. Get HIFIS Intro letter
wget https://gitlab.hzdr.de/hifis/overall/communication/hifis-news/-/jobs/artifacts/main/raw/build/2022-03_overview_de.pdf?job=build -O media/HIFIS_overview_2022_03_de.pdf
wget https://gitlab.hzdr.de/hifis/overall/communication/hifis-news/-/jobs/artifacts/main/raw/build/2022-03_overview_en.pdf?job=build -O media/HIFIS_overview_2022_03_en.pdf

# 3. Get HIFIS Teaser Video
wget https://gitlab.hzdr.de/hifis/overall/communication/hifis-teaser-video/-/jobs/artifacts/main/raw/video/video_full.mp4?job=video_build -O assets/videos/video_full.mp4

# 4. Get Administrative and Technical Documentation
# tbd

# 5. Get content for consulting-handbook.
include_mkdocs \
  "https://gitlab.hzdr.de/hifis/software/consulting/consulting-handbook/-/jobs/artifacts/main/download?job=pages" \
  "consulting-handbook/"

