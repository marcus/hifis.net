---
layout: spotlight

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: CrystFEL
date_added: 2022-08-22

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: crystfel/crystfel-logo.png

# One or two sentences describing the software
excerpt: > 
    CrystFEL is a suite of programs to process 
    data from "serial crystallography" experiments.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image: crystfel-image-1.jpg

# Title at the top, inside the title-content-container
title: CrystFEL

# Add at least one keyword
keywords:
    - crystallography
    - XFEL
    - synchrotron
    - structural biology

# The Helmholtz research field
hgf_research_field: Matter

# At least one responsible centre
hgf_centers:
    - Deutsches Elektronen-Synchrotron DESY

# List of other contributing organisations (optional)
contributing_organisations:

# List of scientific communities
scientific_community:
    - Photon science, X-ray crystallography

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: thomas.white@desy.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: webpage
      link_as: https://desy.de/~twhite/crystfel/
    - type: gitlab
      link_as: https://gitlab.desy.de/thomas.white/crystfel/
    - type: github
      link_as: https://github.com/taw10/crystfel

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: GPL-3.0-or-later

# Is the software pricey or free? (optional)
costs: Free

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    -

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - Web

# List of programming languages (optional)
programming_languages:
    - C, Python

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi: 10.1107/S0021889812002312

# Funding of the software (optional)
funding:
    - shortname: DESY

---

# CrystFEL in a nutshell

[CrystFEL](https://www.desy.de/~twhite/crystfel/)
is a suite of programs to process data from "serial crystallography" experiments.
Datasets from
this type of experiment consist of many thousands of individual diffraction patterns, each one of which
was produced by X-rays scattering from a tiny crystal.  This technique is most often used for investigating
the structure of biological molecules such as proteins, but other types of matter have been investigated
as well.  The X-rays can come from a synchrotron storage ring such as
[PETRA III](https://photon-science.desy.de/facilities/petra_iii/index_eng.html), or from a free-electron
laser such as the
[European XFEL](https://www.xfel.eu/).
Electron beams in a transmission electron microscope can also be
used.  The "serial" mode of data acquisition is particularly relevant for time-resolved experiments -
making so-called "molecular movies".

To process the data, each diffraction pattern has to be analysed individually.
The first step is to find the
sharp "Bragg" peaks in the pattern, from which the orientation of the crystal as well as the geometrical
parameters of the crystal lattice can be determined.
The intensities of the Bragg peaks then need to be
measured, including very weak peaks whose presence can be inferred from the crystal parameters
despite not being found by the initial peak search.
The intensities from all the patterns are then merged
together to produce a final combined dataset which can be used to determine the structure of the
crystal.

CrystFEL contains programs to perform each of these steps, brought together with a graphical user
interface.  High performance computing is often needed due to the sizes of the datasets, so the GUI can
submit jobs directly via a batch system such as Slurm.
Recently, streaming data interfaces (for example,
based on ZeroMQ) have been implemented, enabling real-time data processing without any
intermediate storage of data.

The CrystFEL website contains further information including presentation slides, a screencast, citation
lists, API reference and a detailed tutorial.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/crystfel/Screenshot_crystfel.png" alt="Screenshot of the CrystFEL graphical user interface.">
<span>Screenshot of the CrystFEL graphical user interface being used to examine a serial crystallography
dataset acquired at <a href="https://www.psi.ch/en/swissfel">SwissFEL</a>.</span>
</div>

See also a <a href="https://www.desy.de/~twhite/crystfel/presentations.html"><i class="fas fa-external-link-alt"></i> presentation video about CrystFEL</a>.
