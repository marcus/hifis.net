---
layout: spotlight

name: HZDR Multiphase Addon for OpenFOAM
date_added: 2022-08-01
preview_image: openfoam/openfoam_preview.png
excerpt: >
    The HZDR Multiphase Addon is a software publication released by Helmholtz-Zentrum
    Dresden-Rossendorf according to the FAIR principles (Findability, Accessibility,
    Interoperability, and Reuseability). It contains experimental research work for
    the open-source CFD software OpenFOAM, released by The OpenFOAM Foundation.
    The developments are dedicated to the numerical simulation of multiphase flows,
    in particular to the multi-field two-fluid model (Euler-Euler method).

title_image: openfoam_title.png
title: HZDR Multiphase Addon for OpenFOAM
keywords:
    - Multiphase Flow
    - Numerical Simulations
    - OpenFOAM
    - Computational Fluid Dynamics
    - Finite volume method
    - Multi-field two-fluid model
    - Euler-Euler method
hgf_research_field: Energy
hgf_centers:
    - Helmholtz-Zentrum Dresden-Rossendorf
contributing_organisations:
    - name: The OpenFOAM Foundation
      link_as: https://openfoam.org/
    - name: CFD Direct
      link_as: https://cfd.direct
scientific_community:
    - Energy / MTET
    - Computational Fluid Dynamics
impact_on_community:
contact: f.schlegel@hzdr.de
platforms:
   - type: webpage
     link_as: https://hzdr.de/openfoam
   - type: gitlab
     link_as: https://gitlab.hzdr.de/openfoam
license: GPL-3.0-or-later
costs: free and open
software_type:
    - numerical simulation
    - multiphase flows
application_type:
    - Desktop
programming_languages:
    - C++
    - C
    - CUDA
    - Shell
    - Python
    - Gnuplot
doi:
     - name: HZDR Multiphase Addon for OpenFOAM
       doi: 10.14278/rodare.767
     - name: HZDR Multiphase Case Collection for OpenFOAM
       doi: 10.14278/rodare.811
funding:
     - shortname: HZDR
---

# HZDR Multiphase Addon for OpenFOAM

The majority of the developments for numerical simulation of multiphase flows at
HZDR are implemented in the open source library OpenFOAM. OpenFOAM is the leading
open source software package for numerical simulations of fluid flows
(Computational Fluid Dynamics, CFD) in engineering applications. By signing the
Contributors Agreement with the OpenFOAM Foundation in 2017, HZDR has the unique
opportunity to actively participate in the development of OpenFOAM. An example
is the contribution of the population balance modelling framework in 2017
together with VTT Technical Research Centre of Finland Ltd, which is
continuously developed and improved since.

Besides, HZDR develops an extension for multiphase flows, the [HZDR Multiphase
Addon for OpenFOAM](https://doi.org/10.14278/rodare.767), and a validation
database, the [HZDR Multiphase Case Collection for OpenFOAM](https://doi.org/10.14278/rodare.811),
both published via Rossendorfer Data Repository. Since 2019, HZDR coordinates
the development work for [OpenFOAM_RCS](https://hzdr.de/openfoam-rcs) (RCS - Reactor Coolant System),
which is an extension for OpenFOAM to simulate the primary cooling circuit in a
nuclear power plant. As a member of the [OpenFOAM Process Engineering Consortium](https://openfoam.org/chemical-process-engineering/),
HZDR has established a strong cooperation with the chemical and process
engineering industry to ensure that the research done on multiphase flows fits
industrial needs.

## Highlights

- [OpenFOAM-Hybrid](https://hzdr.de/openfoam-hybrid): A morphology adaptive two-fluid model
- [OpenFOAM_RCS](https://hzdr.de/openfoam-rcs): Sustainable development of simulation software for modeling of reactor coolant systems
- Scientific workflow based on [Snakemake](https://snakemake.readthedocs.io/en/stable/) workflow management
- [more](https://hzdr.de/openfoam)

<!-- <center>
<video src="{{ site.directory.videos | relative_url }}spotlights/openfoam/Simulation-of-a-distillation-column.webm" autoplay controls loop />
</center>
 -->
<div class="spotlights-text-image">
<video src="{{ site.directory.videos | relative_url }}spotlights/openfoam/Simulation-of-a-distillation-column.webm" alt="Distillation column" autoplay controls loop>
<span>Example of a simulation of a distillation column.</span>
</div>

## Contributor with an Addon

The open source library OpenFOAM is written in state-of-the-art, object-oriented
C++ and licensed under GNU GPL v3 license. Excellent code quality and robustness
of the OpenFOAM library is ensured by the maintenance work of the OpenFOAM Foundation,
which is a major requirement for CFD software in industrial design processes.
The OpenFOAM Foundation follows a continuous integration strategy to reduce
the amount of bugs in the source code. This development strategy has higher
demands on downstream developers, like HZDR. Contributions to the release are a
key prerequisite for industrial application of our developments. However, the
demands on software quality are high and cannot always be ensured in research.

Hence, HZDR develops the [HZDR Multiphase
Addon for OpenFOAM](https://doi.org/10.14278/rodare.767), which primarily
includes prototype functionality. The flexible interfaces of OpenFOAM and a
early communication with the core developers of OpenFOAM makes sure the
maintenance generated by the add-on is minimised. A support contract with
CFD Direct ensures that the long-term maintenance and the integration costs
of HZDR's release contributions are covered.

<div class="spotlights-text-image">
<img alt="Illustration of the linkage between OpenFOAM release and HZDR Multiphase Addon for OpenFOAM."
src="{{ site.directory.images | relative_url}}spotlights/openfoam/contributor_with_addon.png">
<span>Illustration of the linkage between OpenFOAM release and HZDR Multiphase Addon for OpenFOAM.</span>
</div>
