---
layout: spotlight

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------
name: MassBank
date_added: 2022-07-14
preview_image: massbank/logo.png
excerpt: >
   MassBank is an open source mass spectral library for the identification 
   of small chemical molecules of metabolomics, exposomics and environmental 
   relevance. 

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
title_image: massbank_logo.png
title: MassBank - High Quality Mass Spectral Database
keywords:
    - database
    - mass spectrometry

hgf_research_field: Earth & Environment
hgf_centers:
    - Helmholtz Centre for Environmental Research (UFZ)
contributing_organisations:
    - name: Leibniz-Institut für Pflanzenbiochemie (IPB)
      link_as: https://www.ipb-halle.de/
    - name: Eawag - Swiss Federal Institute of Aquatic Science and Technology
      link_as: https://www.eawag.ch/en/
    - name: Luxembourg Centre for Systems Biomedicine - University of Luxembourg
      link_as: https://wwwen.uni.lu/
scientific_community:
impact_on_community:
contact: massbank@massbank.eu
platforms:
    - type: github
      link_as: https://github.com/MassBank
    - type: webpage
      link_as: https://massbank.eu/
license: GPL-2.0-or-later
costs: free
software_type:
application_type: Web
programming_languages:
    - Java
    - JavaScript
    - Perl
doi: 10.5281/zenodo.5775684
funding:
    - shortname: UFZ
      link_as: https://www.ufz.de/
    - shortname: NFDI4Chem
      link_as: https://www.nfdi4chem.de/
    - shortname: NORMAN Association
      link_as: https://www.norman-network.net/
---

# MassBank in a nutshell

MassBank is an open source mass spectral library for the identification of
small chemical molecules of metabolomics, exposomics and environmental
relevance. The vast majority of MassBank
[contents](https://massbank.eu/MassBank/Contents) now features high-resolution
mass spectrometry data, although all kinds of mass spectral data are accepted.
A range of [search](https://massbank.eu/MassBank/Search) options are available
for browsing the data. The MassBank library is based on text file records
containing the record metadata and the mass spectral information in the
[MassBank record format](https://github.com/MassBank/MassBank-web/blob/main/Documentation/MassBankRecordFormat.md).
All data is archived on [GitHub](https://github.com/MassBank/MassBank-data) and
[Zenodo](https://doi.org/10.5281/zenodo.3378723); the code is also on
[GitHub](https://github.com/MassBank/MassBank-web). The MassBank library can be
[downloaded in different formats such as text records, database files (sql) and MSP files](https://github.com/MassBank/MassBank-data/releases).

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/massbank/Atrazine_Mass_Spectrum.png" alt="Atrazine Mass Spectrum.png">
</div>

MassBank is maintained and developed by the [MassBank consortium](https://github.com/MassBank)
and is hosted at the UFZ
([Helmholtz Centre for Environmental Research, UFZ, Leipzig, Germany](https://www.ufz.de/index.php?en=40500)).
