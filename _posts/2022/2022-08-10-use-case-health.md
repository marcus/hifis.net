---
title: "AI Research in Health Topics at CASUS"
title_image: CASUS-Center-Advanced-System-Understanding.jpg
data: 2022-08-10
authors:
  - "Bussmann, Michael"
  - klaffki
layout: blogpost
categories:
  - Use-Case
tags:
  - Health
  - Helmholtz AAI
  - OpenStack (HDF Cloud)
  - Rancher managed Kubernetes
  - JupyterHub
  - Artificial Intelligence
excerpt: >
    The use of pseudonymized and anonymized patient data in a secure, federated way to better understand, diagnose and treat cancer is the big next step in enhancing the quality of decision making in clinics beyond what current guidelines can offer.
---

## Insights in current research at CASUS

As an institute of the Helmholtz-Zentrum Dresden-Rossendorf ([HZDR](https://www.hzdr.de/)), the Center for Advanced Systems Understanding ([CASUS](https://www.casus.science/)) is part of the Helmholtz Association. Therefore, the general use of HIFIS infrastructure and services is a good choice, as Michael Bussmann laid down in an [overview post]({% post_url 2022/2022-08-04-use-case-casus %}). Here, he describes a specific use case for HIFIS services from the research field [Health](https://www.helmholtz.de/en/research/research-fields/health/).

## Clinical decision support beyond guideline recommendations based on real world evidence

The use of pseudonymized and anonymized patient data in a secure, federated way to better understand, diagnose and treat cancer is the big next step in enhancing the quality of decision making in clinics beyond what current guidelines can offer. 

CASUS currently is involved in several projects on collecting real world clinical evidence on prostate, lung and breast cancer both from clinical trials and clinical data to enhance the quality of diagnostics, treatment and patient life compared to guideline based decision making.
In these projects, researchers, the guideline organizations as well as clinics and pharmaceutical companies are highly interested in developing biostatistical and artificial intelligence models based on this large collection of data.

The first step in providing an infrastructure for this is a secure, highly available cloud infrastructure with fine-granular authentication and authorization services that allow access and operation in a federated way. 
Specifically, while a central instance will provide the largest single portion of data and compute resources, it will also orchestrate the analysis across different data provider sites both for structured and unstructured data.

The use of standardized data and metadata formats across different data provider sites and cancers, such as the [OMOP](https://www.ohdsi.org/data-standardization/the-common-data-model/) standard, together with the federated software stack for statistical analysis and federated learning can leverage the potential of evidence based decision making in clinics.

### Infrastructure

The infrastructure depicted below is built upon the HIFIS services
[OpenStack (HDF Cloud)](https://helmholtz.cloud/services?serviceDetails=svc-36fef321-6a91-4dc0-aff8-3fd4ebc6008c) and
[Rancher managed Kubernetes](https://helmholtz.cloud/services?serviceDetails=svc-8da9d670-383c-4641-9896-fa25220cc0b5) and provides a
[JupyterHub](https://helmholtz.cloud/services?search=Jupyterlab)
for data analytics workflows.
For secure access and authorization we plan to use the
[Helmholtz AAI](https://aai.helmholtz.de).


<figure>
    <img src="{% link assets/img/posts/2022-08-10-use-case-CASUS-zwei/architecture-ai-development.png %}" alt="tbd" style="float:right;width:100%;min-width:500px;padding:5px 5px 20px 20px;">
    <figcaption>Architecture for AI development and deployment.</figcaption>
</figure>

### Further information on related research

For further information on research focusing on AI knowledge base implementation in order to improve cancer patient treatments, [see also this article <i class="fas fa-external-link-alt" aria-hidden="true"></i>](https://www.casus.science/news/casus-news/improving-cancer-patient-treatments-through-artificial-intelligence/).

## Get in contact
For CASUS: [Michael Bussmann](mailto:m.bussmann@hzdr.de?subject=[HIFIS-use-case])

For HIFIS: [HIFIS Support](mailto:support@hifis.net?subject=[communication])
