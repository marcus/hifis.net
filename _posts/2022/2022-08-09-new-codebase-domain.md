---
title: "GitLab: Helmholtz Codebase switches its domain"
title_image: jingda-chen-4F4B8ohLMX0-unsplash.jpg
date: 2022-08-09
authors:
  - hueser
layout: blogpost
categories:
  - News
tags:
  - Announcement
  - GitLab
  - Domain
  - Codebase
excerpt: >
  On November 11th, 2022, Helmholtz Codebase will be available under the
  new domain
  <a href="https://codebase.helmholtz.cloud">https://codebase.helmholtz.cloud</a>.
  The previous domain
  <a href="https://gitlab.hzdr.de">https://gitlab.hzdr.de</a>
  will redirect to this new domain.
  Nothing else changes for the users.
---

<div class="alert alert-primary" markdown=1>
## gitlab.hzdr.de becomes codebase.helmholtz.cloud
{{ page.excerpt }}
</div>

## The Reason Why

The [Helmholtz Cloud](https://helmholtz.cloud) 
offers cloud services to all members of the Helmholtz Association and partners.
This also includes a 
[GitLab service](https://helmholtz.cloud/services?serviceDetails=svc-c5d1516e-ffd2-42ae-b777-e891673fcb32)
that is made available under the domain <https://gitlab.hzdr.de> by colleagues
of the [Helmholtz-Zentrum Dresden-Rossendorf (HZDR)](https://www.hzdr.de)
and the [Helmholtz-Zentrum für Umweltforschung (UFZ)](https://www.ufz.de).
Meanwhile, the vast majority of users does not come from the HZDR any longer.
We decided to reflect this fact in the choice of the primary domain name.
Therefore, a small contest to find a new domain name for this Helmholtz-wide
GitLab service was initiated.
At the end of this contest, most of the participants voted for
_Helmholtz Codebase_ and <https://codebase.helmholtz.cloud>, respectively,
as the successors of the previous service and domain name.

## The New Domain

The Helmholtz GitLab has already been referred to as the **Helmholtz Codebase**
for quite a while now.
At the moment, you can already access the service via <https://codebase.helmholtz.cloud>.
It will be redirected to <https://gitlab.hzdr.de>.

On **November 11th, 2022** these domains and redirects will be
swapped to make the Helmholtz Codebase available under its new
primary domain <https://codebase.helmholtz.cloud>.

## Which Changes Do the Users Face?

On November 11th, 2022, all users will automatically be redirected from
<https://gitlab.hzdr.de> to <https://codebase.helmholtz.cloud>.
Apart from that, nothing else changes for the users, except for the need to
log in once again.

## What if ...?

In case you are facing issues with the new domain name or the redirect, please
let us know.
In that case, please contact our HIFIS Support via <support@hifis.net>, or
the [contact form]({% link contact.md %}).
Thank you very much!

Happy collaboration and happy coding!
