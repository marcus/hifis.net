---
title: "Gitlab for Require&shy;ments analysis at Earth Data Portal"
title_image: globe.jpeg
data: 2022-10-04
authors:
  - klaffki
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - Use-Case
tags:
  - Earth & Environment
  - Helmholtz Codebase
  - nubes
  - Mattermost
  - Notes
  - Consulting
  - Education
excerpt: >
    Within the DataHub of the Research Field Earth & Environment, an Earth Data Portal is being developed.
    This portal will serve as a central access point for searching and exploring data and thematic ressources from the research field.
    Missing features and functionalities were identified during a requirement analysis, for which the developers made use of the HIFIS Codebase.
    This proved to be an efficient workflow that will be further developed and used in similar projects.
---

## Data for the Earth System

<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right medium nonuniform"
alt="DataHub logo"
src="{% link assets/img/posts/2022-10-04-datahub/datahublogo.svg %}"
/>
</div>
<div class="text-box" markdown="1">

The [DataHub](https://datahub.erde-und-umwelt.de/en/) of the Research Field [Earth and Environment](https://www.helmholtz.de/en/research/research-fields/earth-and-environment/) is a research data initiative across the seven centres of the Helmholtz Programme [Changing Earth - Sustaining our Future](https://earthenvironment.helmholtz.de/changing-earth/program/). Within its three compartemental Hubs ATMO, MARE and TERRA, it aims at the development of a distributed and interoperable research data infrastructure as well as operational procedures and data products for the whole Research Field and the Earth System Sciences in general.

In this context, the DataHub is responsible for implementing state-of-the-art policies, tools and best-practices particularly in the fields of research data management (RDM) and research software development across all participatng centres. It hence contributes to the fullfilment of the [FAIR principles](https://www.go-fair.org/fair-principles/) (Findability, Accessibility, Interoperability, Reusability) and fosters open science in the Research Field Earth and Environment.

One of the main objectives of the DataHub is the joint development of thematic viewers, through which products and information from the participating centres are presented to end-users from the scientific, economic, and governmental sectors. As an example, the so-called [Stakeholer-View](https://datahub.erde-und-umwelt.de/en/) integrates prominent and widely-used products like the [German Drought Monitor](https://datahub.erde-und-umwelt.de/en/s/drought) or the [Global Earthquake Viewer](https://datahub.erde-und-umwelt.de/en/s/earthquakes).


## Making use of HIFIS Software and in-person Services
As such an initiative requires dedicated tools and services for an efficient and productive collaboration across several research centres, the DataHub has been one of the first adopters of the HIFIS-services:
- The exchange of documents, presentations, protocols and other files is conducted through the HIFIS Nextcloud service [nubes](https://helmholtz.cloud/services?serviceDetails=svc-fa7ffed8-ea57-4a14-b6bd-abd46bcec6c5).
- [Mattermost](https://cloud.helmholtz.de/services?serviceDetails=svc-1be91786-b7e7-4fa3-81d9-1b95dd03cd52) is used for inter-institutional communication via private or dedicated team and group chats
- Collaborative notes and protocols are developed within the Markdown editor [Notes](https://cloud.helmholtz.de/services?serviceDetails=svc-cea6f848-b46b-4340-92fe-17da42a78829).
- The HIFIS GitLab Instance [Codebase](https://gitlab.hzdr.de/users/sign_in) serves as a central and widely adopted framework for collaborative software development.
- The seamless and transparent usage of all these services is realized through HIFIS' central Authentication and Authorisation Infrastructure [Helmholtz AAI](https://aai.helmholtz.de)

Apart from those software services, the DataHub is heavily promoting trainings through [HIFIS Education](https://hifis.net/services/software/training) for collaborative software development via GitLab and other fundamental [software engineering skills](https://events.hifis.net/category/4/) as well as consultation and support for software licensing issues through the [Software Engineering Consulting](https://www.hifis.net/services/software/consulting.html).


## Earth Data Portal
One major DataHub project is the joint development of an Earth Data Portal. This portal will serve as a central access point for searching and exploring data and thematic ressources from the research field Earth & Environment and beyond. This will be realized by tailor-made harvesting and mapping from a wide range of existing data archives and repositories like [Pangaea](https://www.pangaea.de), the [World Data Center for Climate](https://www.wdc-climate.de) at the [DKRZ](https://www.dkrz.de/de) or [RADAR4KIT](http://radar.kit.edu) as well as institutional repositories into a single portal infrastructure. The technical basis for the portal is the well-established [Marine Data Portal](https://marine-data.de/), which is now enhanced by community-driven functionalities.

### Using GitLab Issues in requirements analysis

<div class="floating-boxes">
<div class="image-box align-left">
<img
class="left large nonuniform"
style="border 2px solid #005AA0"
alt="Screenshot of the feature request template"
src="{% link assets/img/posts/2022-10-04-datahub/Screenshot_FeatureIntegration.png %}"
/>
</div>
<div class="text-box" markdown="1">

The missing features and functionalities for a data portal covering the entire research field were identified during a requirement analysis, for which the developers made use of the HIFIS Codebase. In particular, by providing dedicated and well-defined [issue templates](https://gitlab.hzdr.de/help/user/project/description_templates.md#description-templates) for *feature requests* and *data / repository integration*, a clear and consistent structure is realized so that both the users and developers can easily add their suggestions, requirements and wishes.

The *feature request template* comes with a brief user story, in which a typical use case for a specific feature or functionality is described (e.g., I want to search for all environmental data in a certain region). By adding a set of pre-defined acceptance criteria, both the developers and the creator of the request can check the status of the feature implementation and close the request once all criteria are met.

The *data integration template* contains pre-defined tasks for providing all needed information like data sources (e.g., through a link to a repository), applied metadata schemas, interfaces as well as information about the data license. This, finally, allows for a convenient and efficient integration of new repositories and other data sources.

Overall, the developed requirement analysis via the HIFIS Codebase has proven to be an efficient workflow that will be further developed and used in similar projects.


## Get in contact
For the Earth and Environment DataHub:
[Peter Braesicke](mailto:peter.braesicke@kit.edu) (KIT), [Jan Bumberger](mailto:jan.bumberger@ufz.de) (UFZ), [Sören Lorenz](mailto:slorenz@geomar.de) (GEOMAR)

For the DataHub Portal:
[Marc Hanisch](mailto:marc.hanisch@gfz-potsdam.de) (GFZ), [Tilman Dinter](mailto:Tilman.Dinter@awi.de) (AWI), [Roland Koppe](mailto:roland.koppe@awi.de) (AWI), [Robin Heß](mailto:robin.hess@awi.de) (AWI), [Christof Lorenz](mailto:christof.lorenz@kit.edu) (KIT)

For HIFIS: [HIFIS Support](mailto:support@hifis.net)
