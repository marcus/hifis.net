---
title: "HIFIS Evaluation: First feedback"
title_image: jingda-chen-4F4B8ohLMX0-unsplash.jpg
data: 2022-10-06
authors:
  - jandt
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
excerpt: >
  After three years of ramp-up, HIFIS underwent a thorough evaluation by an international expert commitee.
  First results are very positive, and we will adapt our roadmap and next action points according to the feedback.
  Stay tuned!
---

# Evaluation after first years of ramp-up

<div class="floating-boxes">
<div class="image-box align-right">
<img
class="right large nonuniform"
alt="webODV logo"
src="{% link assets/img/posts/2022-10-06-hifis-evaluation/HIFIS_Eval.jpg %}"
/>
</div>
<div class="text-box" markdown="1">
HIFIS, the Helmholtz-wide platform for federated IT services, underwent a thorough review during August and September 2022, with the [major event taking place at the coordinating centre DESY from Sept 29-30](https://events.hifis.net/event/450).

The evaluation commitee represents national and international institutions and collaborations, such as the
[European Open Science Cloud](https://www.eosc-portal.eu/)
and multiple more.
External viewpoints, e.g. from
[Open Science Office](https://os.helmholtz.de/en/),
[Helmholtz Metadata Collaboration](https://helmholtz-metadaten.de/),
[NFDI](https://www.nfdi.de/), and
[DFN](https://www.dfn.de/),
as well as in-depth discussions with members of the [HIFIS Scientific Advsory Board]({% link structure/members-sab.md %}),
helped the reviewers to obtain a possibly complete picture of the landscape where HIFIS is embedded into, and its interfaces to other digital initiatives.

</div>
</div>
<div class="clear"></div>

## What has been presented

During two day's sessions, the [progress of HIFIS](https://events.hifis.net/event/450/timetable/#20220929.detailed), as well as the
[plans for potential future development](https://events.hifis.net/event/450/timetable/#20220930.detailed),
have been presented and discussed in detail.
HIFIS has been establishing the technical and procedural frameworks to allow all Helmholtz Centres to seamlessly share digital resources with all Helmholtz and collaboration partners.
Meanwhile, more than 20 cloud services have been brought online, with more than [11k users registered, numbers strongly rising](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci/-/jobs/722376/artifacts/file/plots/202102_Backbone/plot_2.pdf).
HIFIS education courses were received comparably well, with more than [14k overall participant hours so far](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci/-/jobs/722376/artifacts/file/plots/pdf/software_workshops-plot_3.pdf).

## Initial feedback

According to the preliminary response by the reviewers,
the achieved progress of all three HIFIS clusters has been rated excellent.
Multiple of the future plans of further strengthening the HIFIS portfolio have been strongly supported.
The enthusiasm of the HIFIS team, spanning practical and efficient cooperation throughout Helmholtz, has been especially acknowledged.

The full review report is expected to be finalised in the next weeks.
Further, we will derive action points for HIFIS, adopting our next steps and [roadmap]({% link roadmap/index.md %}) according to reviewer's suggestions.

### Stay tuned

On **Nov 18, 09:30 until noon**, we will hold an online [**Post Review Meeting**](https://events.hifis.net/event/565/).
All Helmholtz and HIFIS stakeholders are invited to discuss with us the results and next steps.
Please [register](https://events.hifis.net/event/565/registrations/571/) to take part and receive detailed information in time.

## Comments? Suggestions?
Contact us anytime if you have any suggestions or questions: <support@hifis.net>
