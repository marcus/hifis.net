---
title: "KIT supports distributed protein computing projects"
title_image: default
date: 2020-04-08
authors:
  - jandt
layout: blogpost
categories:
  - News
excerpt:
  Since end of March, the Grid Computing Centre Karlsruhe and the HPC systems
  of KIT support the distributed computing projects Folding@Home and Rosetta@home,
  helping to improve the understanding of proteins, including those of SARS-CoV-2.
lang: en
lang_ref: 2020-04-08-kit-supports-distributed-protein-computing-projects
---

Since end of March, the
[Grid Computing Centre Karlsruhe](http://www.gridka.de/)
and the
[HPC systems of KIT](https://www.scc.kit.edu/en/services/12055.php)
support the distributed computing projects Folding@Home and Rosetta@home,
helping to improve the understanding of proteins, including those of SARS-CoV-2.

<a type="button" class="btn btn-outline-primary btn-lg" href="https://www.scc.kit.edu/en/aboutus/13531.php">
  <i class="fas fa-external-link-alt"></i> Read more
</a>
