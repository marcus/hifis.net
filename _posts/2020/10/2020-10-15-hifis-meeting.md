---
title: "Preparing HIFIS Annual Meeting Oct 21-22 - Schedule and Registration"
title_image: omar-flores-MOO6k3RaiwE-unsplash.jpg
date: 2020-10-15
authors:
  - jandt
  - spicker
layout: blogpost
categories:
  - Announcement
redirect_from:
  - announcement/2020/09/29/hifis-meeting
  - announcement/2020/09/29/hifis-meeting.html
excerpt: >
    <strong>Update:</strong> From Oct 21 (13:30) until Oct 22 (13:00 max.), we are going to hold our second all-HIFIS meeting. <a href="https://events.hifis.net/event/25/">Click here for the Indico page.</a>
    Over the course of two half days, we will report on the progress of HIFIS, discuss ongoing work packages and also seek to incorporate new ideas, thoughts and queries from non-HIFIS colleagues in Helmholtz.

---

# HIFIS Meeting 2020 is coming!
From Oct 21-22, we are going to hold our second all-HIFIS meeting. It will be held as video conference.

Over the course of two half days, we will report on the progress of HIFIS, discuss ongoing work packages and also seek to incorporate new ideas, thoughts and queries from non-HIFIS colleagues in Helmholtz.

#### Time plan
- Meeting times: Wed, **Oct 21 13:30-17:00** and Thu, **Oct 22 09:00-13:00** max.
- **Until 15:45**, we will start with high level presentations on the status-quo of HIFIS. After that, we'll move towards more technical and open discussions on ongoing works and further developments.
- For details on access, time plan and contributions, [**see our Indico page**](https://events.hifis.net/event/25/). 
- [Registration](https://events.hifis.net/event/25/registrations/23/) is encouraged, especially if you want to take part in the Open Topic discussions (see below).
- You are free to join whenever you are interested in certain topics.



#### Open Topics
We want to allow inclusion of ideas and thoughts of colleagues outside HIFIS. Thus we will hold discussion / brainstorming sessions on HIFIS-related topics provided from colleagues in Helmholtz. 

- A detailed view on the topics and breakout sessions [can be found here](https://events.hifis.net/event/25/sessions/40/#20201021).
- In case you want to take part in these discussion sessions, we ask you to [register](https://events.hifis.net/event/25/registrations/23/) to facilitate distribution of participants.


# Further information

#### Contact
In case anything breaks, you can directly [contact the organization team](mailto:contact@hifis.net).

You are further invited to [register here for the HIFIS announcement letter](https://lists.desy.de/sympa/subscribe/hifis-newsletter).

#### Misc
This post is an update of the initial announcement of 2020-09-28.

This all-HIFIS meeting is the second of its kind, after the HIFIS Conference 2019. [Click here for the presentations of 2019](https://indico.desy.de/event/23411/material/slides/).


