---
title: "Announcing the HIFIS Event Management Service"
title_image: default
data: 2021-04-27
authors:
  - Erxleben, Fredo (HZDR)
layout: blogpost
categories:
  - News
excerpt: >
    Last year we introduced a rough-and-ready solution to offer event management solutions via Indico.
    Now we can finally introduce you to the shiny new HIFIS Events service with Helmholtz AAI connectivity.
---

**TLDR:** HIFIS Events is now online at **<https://events.hifis.net>** and can be used by all in Helmholtz - Yay!

## What is this about?

When we started organizing events in the scope of HIFIS (mostly workshops) it became clear that we needed a single event organization service that can be used across all of Helmholtz.
It quickly became apparent that [Indico][indico] would be the best-fitting solution for this purpose.
We had not yet decided at this point who would host the service and be responsible for maintaining it, but due to the urgency of the matter, a decision was made to set up an intermediate solution first and clarify the remaining questions later.

Now all these open points have been dealt with and we could finally put a proper solution in place for all Helmholtz members to enjoy – carefully maintained by our esteemed colleagues at the DESY.

## What is new?

We have taken the liberty to reorganize the group structure to better cope with the huge variety of events that take place within the Helmholtz universe.
Broadly speaking, we grouped them into three categories:

* **Research field-specific:** Events that cross centre boundaries and which are mainly aimed at the research community in general (conferences for example);
* **Platform-specific:** Events that originate from one of the many Helmholtz platforms and can be either internal or aimed at the broader Helmholtz audience (Our workshops come to mind);
* **Centre-specific:** Events which are hosted at one of the centres for predominantly internal affairs.

There is also a _Test_ category which you can freely use to experiment with event setups.

If you are missing a fitting category or can not decide, feel free to contact the [HIFIS support][hifis-email] and we will be happy to help you out. 

## What about the users and user accounts?

Login to the Events platform is done via Helmholtz AAI. We are working on shortening the workflow; currently you need to click on "Login", "Keycloak/Helmholtz AAI", and again "Helmholtz AAI". 
Then you can select your identity provider / institution. 
This works for most Helmholtz centres and sometimes for centres outside of Helmholtz.
If this does not work, you can authenticate via Social providers, e.g. Github, ORCID, Google.
[Contact us][hifis-email] if anything fails.

We have transferred all user accounts (including reservations). 
If you already had a user account and used a Helmholtz e-mail address to register, you can now log in via the Helmholtz AAI.
On your first visit via the AAI, you will then be informed that you account will be connected to the AAI login.
If you plan to maintain your rights to create events or upload data, **please log in via Helmholtz AAI as soon as possible and connect your former local account.**

Note that the creation of new local/Indico accounts is deactivated. 

## What about current events?

All events, past and current have been transferred including all associated files and registrations.
If you are managing an event we recommend to check if everything still is as it should be. 

## What about the links I saved earlier?

We created a redirection from the old to the new instance, so all links are still valid.
However, this will only be the case until August 01, 2021, so **please remember to update your bookmarks**, by replacing the old URL `hifis-events.hzdr.de` by the new `events.hifis.net`.

## Who to call?

~~Ghostbusters!~~ 

If you have any inquiries, you can reach the HIFIS support team via a [web-form][hifis-helpdesk] or [write us an e-mail][hifis-email].

[hifis-events]: https://events.hifis.net
[indico]: https://getindico.io/
[hifis-helpdesk]: https://hifis.net/contact
[hifis-email]: mailto:support@hifis.net?subject=[events]
