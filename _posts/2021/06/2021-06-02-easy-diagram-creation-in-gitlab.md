---
title: "Easy Diagram creation in Gitlab"
title_image: clark-tibbs-oqStl2L5oxI-unsplash.jpg
date: 2021-06-02
authors:
  - huste
  - jandt
layout: blogpost
categories:
  - Tutorial
redirect_from:
  - tutorials/2021/06/02/easy-diagram-creation-in-gitlab.html
excerpt: >
    Recently, the Helmholtz-wide software development platform (<a href="https://gitlab.hzdr.de">Gitlab</a>) has been extended
    with the ability to create diagrams from textual descriptions.
    This post will help you getting started with this new feature.

---

{{ page.excerpt }}

## What is it about and where can it be applied?
Gitlab is perfect for colloborative work on codes, scripts and other shared files.
Now you can also create virtually all sorts of diagrams and sketches directly in Gitlab,
both allowing to **quickly create plots**, as well as
collaboratively build and maintain **complex graphs**.

All graphs are built from simple scripts directly in Gitlab using a plugin named [Kroki](https://kroki.io/).
Such scripts can be used in any place on the [Helmholtz-wide Gitlab](https://gitlab.hzdr.de)
where Markdown is supported:
  * Markdown documents inside repositories
  * Comments
  * Issues
  * Merge requests
  * Milestones
  * Snippets (the snippet must be named with a `.md` extension)
  * Wiki pages

<a href="https://gitlab.hzdr.de/examples/kroki"
                            class="btn btn-outline-secondary"
                            title="Link to examples in Gitlab"> 
                            <i class="fas fa-lightbulb"></i> Click here for examples in Gitlab!</a>

## Quick-Start Example
Let us jump right in with an example by creating a sequence diagram, using the common Unified Modeling Language (UML).

Put this content describing the diagram in any of the items
listed above.

    ```plantuml
    @startuml
    participant User

    User -> A: DoWork
    activate A

    A -> B: << createRequest >>
    activate B

    B -> C: DoWork
    activate C
    C --> B: WorkDone
    destroy C

    B --> A: RequestCreated
    deactivate B

    A -> User: Done
    deactivate A
    @enduml
    ```

This description is then converted into this sequence diagram.

![PlantUML sequence diagram](https://kroki.hzdr.de/plantuml/svg/eNpljkEKwzAMBO_7Cn0gHwjBYDsvKJSejaODKcSpohb6-1pOIYVedNDuzu6WREsuW1qVrjsLYJcGR36kud6q3JGylldSJg94k8JI00RZuD0v_HjyruTc6QtAMF_8R0REGg6ECXNdGUvLS303zWJH9Rcbe8fSLL_wPsJ2Gr8DzokfZu9AWw==)

## How Does That Work?
In the background a tool called [Kroki](https://kroki.io) is doing the magic.
It takes the textual description and converts it into an image.
The example shown above is using [PlantUML](https://plantuml.com/) to create
the diagram of our choice (see line 1).

So all you need to do is to create the textual description of your diagram and
put it into GitLab Markdown.
When viewing the content, in the background we will make sure that you are
presented an image of your diagram description.

## Step-By-Step Guide
In this example our goal is to create a visual representation of JSON data.
Let's take this example JSON data.

<details>
  <summary>Show JSON data</summary>
  <div markdown=1>
```json
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 28,
  "address": {
    "streetAddress": "Bautzner Landstr. 400",
    "city": "Dresden",
    "state": "Saxony",
    "postalCode": "01328"
  },
  "offices": [
    {
      "building": 512,
      "room": 1234
    },
    {
      "building": 312,
      "room": 3
    }
  ],
  "departments": []
}
```
  </div>
</details>

We will use PlantUML to [display the JSON data](https://plantuml.com/json).

At first, we need to tell GitLab that we want to create a diagram using
PlantUML.
This is why we create a Markdown code block using PlantUML for syntax highlighting.

    ```plantuml

    ```

The [PlantUML documentation](https://plantuml.com/json) tells us how such a
visual representation can be created.
In this case you need to embed the JSON data into a block starting with
`@startjson` and ending with `@endjson`.

    ```plantuml
    @startjson
    {
      "firstName": "John",
      "lastName": "Smith",
      "age": 28,
      "address": {
        "streetAddress": "Bautzner Landstr. 400",
        "city": "Dresden",
        "state": "Saxony",
        "postalCode": "01328"
      },
      "offices": [
        {
          "building": 512,
          "room": 1234
        },
        {
          "building": 312,
          "room": 3
        }
      ],
      "departments": []
    }
    @endjson
    ```

You will be presented an image as shown below.

![PlantUML JSON example](https://kroki.hzdr.de/plantuml/svg/eNp1kDurAjEQhfv9FSH1IvtQECvv1UrExlIsopnVXHYnkozgg_3vThJWC7lNIN85M-cwc0_K0Z-3mD0zIWRjnKeN6kDOhFzZM8o84FZ96LYzdE5YnQKppumjtQPvGYRNDDw5APp5Y_mrrvRAcGKtULM6EuOiiJvYfTR0D6YluzXggLkfpVh1s3gf8MWy0C6sjlpR1tVUstLHJrZpzBFC5C66Ux8WDlfTaoMnViZllQ_YWdsxKqt6HFGf_ztWf4_VaYbffUzXcOGTdoAUG-yzPpsD6nDjF8HSYBU=)



### Gantt Diagram
PlantUML also supports Gantt diagrams:
<details>
  <summary>Show code</summary>
  <div markdown=1>
    ```plantuml
    @startgantt
    printscale weekly

    Project starts the 1st of July 2021
    [Literature Review] lasts 4 weeks
    [Literature Review] is 100% completed
    then [Run Experiments] lasts 12 weeks
    [Run Experiments] is 70% completed
    -- Phase Two --
    then [Data Analysis] lasts 8 weeks
    [Data Analysis] is 0% completed
    then [Write Publication] lasts 6 weeks
    [Write Publication] is 0% completed
    @endgantt
    ```
  </div>
</details>

![Sample Gantt Diagram with PlantUML](https://kroki.hzdr.de/plantuml/svg/eNpt0DFrw0AMhuH9fsW3ZDTYJrQZU0iWksGEQoeQQXXU5NLLnTnJdf3ve5heoalnvTxIWotS1DN5VdNF61VacoyB-cONxjQxXLlVTJVAL4xKFOEdz70bUZd1ZQ47qxxJ-8jY86fl4QhHkvLl5MhsYQVVWS7QhlvnWPlkEu5x2Pce26-Oo71x2iZTVZ2tf0GSHv9ARYHmQsJ4GQKK4gfekBKePLlR7C-7yurdNJkzu73GdAea_s3ZltQGn5mHzMwU99Sa_Wn69zeQE4Rw)

More in-depth information on the Gantt diagram syntax is available in the
official [PlantUML documentation](https://plantuml.com/de/gantt-diagram).

## What Else Can Be Done?
The new feature is not limited to using PlantUML.
The new integration gives you access to more than that.
It supports, among others, [Vega](https://github.com/vega/vega),
[C4 with PlantUML](https://github.com/RicardoNiepel/C4-PlantUML),
[GraphViz](https://www.graphviz.org/) or
[BlockDiag](https://github.com/blockdiag/blockdiag).
For a complete list please refer to the official
[list of supported diagram types and formats](https://kroki.io/#support).


### Vega
#### Word Cloud
The following wordcloud is generated from the
[mission statement of the Helmholtz Association](https://www.helmholtz.de/en/about-us/the-association/mission/)
using [Vega](https://vega.github.io/vega/).

<details>
  <summary>Show code</summary>
  <div markdown=1>
    ```vega
    {
    "$schema": "https://vega.github.io/schema/vega/v5.json",
    "description": "A word cloud visualization depicting the mission statement of Helmholtz.",
    "width": 800,
    "height": 400,
    "padding": 0,

    "data": [
        {
        "name": "table",
        "values": [
            "We contribute to solving the major challenges facing society, science and the economy by conducting top-level research in strategic programmes within our six research fields: Energy, Earth & Environment, Health, Aeronautics, Space and Transport, Matter, and Information. We research highly complex systems using our large-scale devices and infrastructure, cooperating closely with national and international partners. We contribute to shaping our future by combining research and technology development with perspectives for innovative application and provisions in tomorrow's world. We attract and promote the best young talents, offering a unique research environment and general support throughout all career stages.",
            "The Helmholtz Association performs cutting-edge research which contributes substantially to solving the grand challenges of science, society and industry. Helmholtz Association scientists focus on researching the highly-complex systems which determine human life and the environment, for example: ensuring that society remains mobile, ensuring that society has a reliable energy supply, ensuring that future generations live in a secure ecosystem, and developing treatments for previously incurable diseases.",
            "The Helmholtz Association performs cutting-edge research which contributes substantially to solving the grand challenges of science, society and industry. Helmholtz Association scientists focus on researching the highly-complex systems which determine human life and the environment, for example: ensuring that society remains mobile, ensuring that society has a reliable energy supply, ensuring that future generations live in a secure ecosystem, and developing treatments for previously incurable diseases.",
            "The activities of the Helmholtz Association focus on securing the long-term foundations of human life and on creating the technological basis for a competitive economy. Our potential, as an Association, to achieve these goals is due to the outstanding scientists working at our 18 major research centres, a high-performance infrastructure and modern research management.",
            "Our scientists develop national research programmes for each of these fields and international experts review these programmes. These expert evaluations form the basis of the programme-oriented funding (POF) which finances to Helmholtz Association research.",
            "Within the six research fields, Helmholtz scientists cooperate with each other and with external partners - working across disciplines, organisations and national borders. Indeed, the being part of the Helmholtz Association means making concerted research effort in which networks form the key to inquiring thought and action. Concerted research is efficient and flexible. The Helmholtz Association uses this research to create an effective basis for shaping the future."
        ],
        "transform": [
            {
            "type": "countpattern",
            "field": "data",
            "case": "upper",
            "pattern": "[\\w']{3,}",
            "stopwords": "(i|me|my|myself|we|us|our|ours|ourselves|you|your|yours|yourself|yourselves|he|him|his|himself|she|her|hers|herself|it|its|itself|they|them|their|theirs|themselves|what|which|who|whom|whose|this|that|these|those|am|is|are|was|were|be|been|being|have|has|had|having|do|does|did|doing|will|would|should|can|could|ought|i'm|you're|he's|she's|it's|we're|they're|i've|you've|we've|they've|i'd|you'd|he'd|she'd|we'd|they'd|i'll|you'll|he'll|she'll|we'll|they'll|isn't|aren't|wasn't|weren't|hasn't|haven't|hadn't|doesn't|don't|didn't|won't|wouldn't|shan't|shouldn't|can't|cannot|couldn't|mustn't|let's|that's|who's|what's|here's|there's|when's|where's|why's|how's|a|an|the|and|but|if|or|because|as|until|while|of|at|by|for|with|about|against|between|into|through|during|before|after|above|below|to|from|up|upon|down|in|out|on|off|over|under|again|further|then|once|here|there|when|where|why|how|all|any|both|each|few|more|most|other|some|such|no|nor|not|only|own|same|so|than|too|very|say|says|said|shall)"
            },
            {
            "type": "formula", "as": "angle",
            "expr": "[-45, 0, 45][~~(random() * 3)]"
            },
            {
            "type": "formula", "as": "weight",
            "expr": "if(datum.text=='VEGA', 600, 300)"
            }
        ]
        }
    ],

    "scales": [
        {
        "name": "color",
        "type": "ordinal",
        "domain": {"data": "table", "field": "text"},
        "range": ["#005aa0", "#8cb423", "#5a696e"]
        }
    ],

    "marks": [
        {
        "type": "text",
        "from": {"data": "table"},
        "encode": {
            "enter": {
            "text": {"field": "text"},
            "align": {"value": "center"},
            "baseline": {"value": "alphabetic"},
            "fill": {"scale": "color", "field": "text"}
            },
            "update": {
            "fillOpacity": {"value": 1}
            },
            "hover": {
            "fillOpacity": {"value": 0.5}
            }
        },
        "transform": [
            {
            "type": "wordcloud",
            "size": [800, 400],
            "text": {"field": "text"},
            "rotate": {"field": "datum.angle"},
            "font": "Helvetica Neue, Arial",
            "fontSize": {"field": "datum.count"},
            "fontWeight": {"field": "datum.weight"},
            "fontSizeRange": [12, 56],
            "padding": 2
            }
        ]
        }
    ]
    }
    ```
  </div>
</details>

<details open="true">
  <summary>Show rendered plot</summary>
  <div markdown=1>
  ![Sample Vega diagram](https://kroki.hzdr.de/vega/svg/eNrtWFuPG7cVfvevIJSisgutvL6s4RrIg1G4TR5SF41RPzj7QM1QGiac4YTkSJbNzW_v9x2ORtLu2ujlNYBEDsnDc7_NfH6g1OwPsWpMq2ev1KxJqY-vHj_emo1ebmxqhtXS-scFQHYfb6-WP0ffzRa8WptYBdsniw1cf612PtSqcn6o1dbGQTv7SfNU1aa3VbLdRqXGqNbGyN2YdDKt6ZLya_WdcW3jXfq0LMh3tk4N0L68vJR1Y-ymSdh4Pm70uq6BETtYCzs6UYwPeFbqs4zY7XRryF3SK2cEtWxvtRtMnMBl771Rle9SsKshGZW8it5tJ6b1zz6oqtHOmW5jolrrimfRV9ak_UJFzF1llO5quWCAy7d7tdoTaz2M8vv-wpmtcSqYaHSoGmWpiQBVbGyl-uA3QbctCOxgAZz5IahoPx7h19a4Or5SbzoTNiD8RofUqD9ivbXBd1ToAurULjUL9dpgSw_JVnGhfuz1yOC7oLvY-wDIH3RKJixk-_tu7UMrNlsqqGOi2UD5joK0vTMfVdxHWC6qIVImcuh02JiLWGlnYO2trSAAMdpuHTTEg_hDMAtg8L2BsLwHT4kGWCmo6oSqduMtsDTt9BAQskZh6ZaFGt0fWFgPJFH03a5sx_1JALGKqZrOO7_Zk0XjfC_OJ-TBVOwNbLSlaWFp23V-q7lWuu-drYonEw9sBPfGKtJ2ybc-BL-bR_q_q4VL6DToKh3AW0924RQrE5Pa-4GeAFV1CVbx67UJZFarobO_DidqN0eTCqqNgR6gkTj0NB5QBj9sGj_g2DlV6WBMYFzBQZeTs8O134H2FGHqdaTXFoEgOG0eVTUkWuXC1JsTDnaNxXhUegTtFQh0yYLi_naUwHfB5kmUILLHwFgcQmW0cD3ALfbLL7All5KNidaoBiDqJqYOxIpTXtx2ysJybeBCre0ANrS6U86uT2LzNFRobfNRE8krnMQhFAI6TRwHJEALc7d-ZR0kuR-q0fB5wDrLXKOMxKfYyu1v3xmdtRg0iS85-hocSqtoKh4igxSZSnCOPisogtGJ3Bdf7QMizg8R9rAdrgr52kJb8Xc_-N0PvuAHmtnOJlusk77oGZPehZ2D0p2Hj1C1OB-6euQdiG6pGfcqcnm4NyVhZFSnVjrawruW4mKSlZQ7Fs-leovE3iN7ip9BflaVU-4W9DwNX4BSiD9Cl147ZOao6kFqBKkiQ9JZa6nYR5dCwv5FMm-SCvLk5VjmJ7evAIkFCIuXXYxRolnoz0ubCNv6GnXreB2ASMU00Jn2KdQJF6NBjyVwun_SDYh3QtDRVpCz9AH31EvzEWwCL73B7EboI6qleic7BUwZtkKj-ShbqVNil9EtpqsXPpBpU8Ntiy4f_uPtXx-Nkba2HfUSqfP7Pekg15ky3pc2h4TuaXMWJ6hOVHZoI0yp3kUxQBFEHWXvo-jk2D6oi6O9q-BjZGhUFrW9o4V92OjOxlETRDMpdIW-VvqP77vamHoxlnJiIvKvh09rNDOGFsJw6wpKhwaPNX69ZimHCooaO5PI5okxfjGSX23362DH-GPVLy0Bo5jd2l_uYoYFgdyK1gR2jfRokRPEA77A7hBpwcbGIx7QlhCmjxNjaZNOYvfQhZHZktCWs9HA11PHndhyUqazpvvz9ESQfS-9eoWMknrpS7sTVwGEOAVBpNk_O6qQ5HiCNGvC-dEBFU4__PTTbn79-dni5hwkojHn6wtfCWYPbW5Nbvf4oUFd553JQ8zIEPzLA7bRKGZ0cvwHGWIZeWF_BGlMbmyLf-Qsp5F7JvAfZeCmTfhF_rmCJvccWg42lDHKzoh3h-qRxWMwev5bDtFkGi-zuGSJfIzc1W3GNhrEvNO4bfCw4s90WTw5N3oLtnDW6JoL7tUePxCrbY0H7uysc3nnB1dDDJkq3eVKnsQps523lH8eKOQ8Uto55ZqTKncpG2c7B0FCYsLJdjzZ8qSWg5oYasFQE6QuEDUAwAUhMOEQYyzTTkaBwmxjN0-UmRPElsmUZVOWlLrMNSdKW2YZrWzufBkhIx_g7WU6bFR6HDufiiq4btHXcHaGotMe1EDj58V2czG9kaMy7xpwItO43BOGLxZZZygZcJjqjPYr23X2AYar9EDbxoyAsY7-4Ez26wzjr_YZ0ZaZC7NeoQBmvWHzghNkGNodVcPn8RUi11LacYZLwLhGwPDalj7i_C4DdI0XmTz0-PkOGtoRQyZiLPESkwEcwEjNmySV10OgcOQcEMhPInIRWMQtwlJUCprRLULCfV4hlWfm9Lw2u9ySodaDccnwOXoEZxxw2nn8Qqbafef2mSxFzVPKRZV5n8HUHrvyhzdqS4cCpUezKf5vFl_PR8xag0O-UTMtCUJ3m5OPCQKLahokwVw8v1qoy4V6fnX94bffHrIR9u3DR-pP6tmj6_-D5q58ALmXqF0_REIc2mVC1fv22_m_3vzt9XyhXlyCj2eXl6eiHvLyg8Pqunw9kVf3-OXvJxV6tmNandhEykTZd8cDCAvT4-jz4YPM4dPLSfYmm7NJAzPoaENsH2bfXF5eaX1J4G9eVqvnT5_J45V-8ecXZnaH6VajUN7l-cCckJmo0H3v4evIBt5O0L8R5NidsN0JZ1tqxEtMXxBIgLSzm6IH-dYkOizYzuFQRg1bkHNQ7fpGI1BtdQt8jQQsoGKwE8vcUe99robqCOnNbYGI9G2vK5v2Z2w8uR9Jw0j_T3FcLq9u7rjfzf_SFrA-y9fFW6XbfhLn4ZdCfh28XvzXtgo-jVo56zAQUCXSb5kAb8GE-I6VGBbS6u9mwNvg62BPImEC_bHwdwe1dDn3oH5_-NJ558aYAm7uJfHPQxA9ebpQVy-ub3VAh4-lT7-WCh7c_Btgeolr)
  </div>
</details>

#### Bar Chart
<details>
  <summary>Show code</summary>
  <div markdown="1">  
    ```vega
    {
      "$schema": "https://vega.github.io/schema/vega/v5.json",
      "description": "A nested bar chart example, with bars grouped by category.",
      "width": 300,
      "padding": 5,
      "autosize": "pad",
    
      "signals": [
        {
          "name": "rangeStep", "value": 20,
          "bind": {"input": "range", "min": 5, "max": 50, "step": 1}
        },
        {
          "name": "innerPadding", "value": 0.1,
          "bind": {"input": "range", "min": 0, "max": 0.7, "step": 0.01}
        },
        {
          "name": "outerPadding", "value": 0.2,
          "bind": {"input": "range", "min": 0, "max": 0.4, "step": 0.01}
        },
        {
          "name": "height",
          "update": "trellisExtent[1]"
        }
      ],
    
      "data": [
        {
          "name": "tuples",
          "values": [
            {"a": 0, "b": "a", "c": 6.3},
            {"a": 0, "b": "a", "c": 4.2},
            {"a": 0, "b": "b", "c": 6.8},
            {"a": 0, "b": "c", "c": 5.1},
            {"a": 1, "b": "b", "c": 4.4},
            {"a": 2, "b": "b", "c": 3.5},
            {"a": 2, "b": "c", "c": 6.2}
          ],
          "transform": [
            {
              "type": "aggregate",
              "groupby": ["a", "b"],
              "fields": ["c"],
              "ops": ["average"],
              "as": ["c"]
            }
          ]
        },
        {
          "name": "trellis",
          "source": "tuples",
          "transform": [
            {
              "type": "aggregate",
              "groupby": ["a"]
            },
            {
              "type": "formula", "as": "span",
              "expr": "rangeStep * bandspace(datum.count, innerPadding, outerPadding)"
            },
            {
              "type": "stack",
              "field": "span"
            },
            {
              "type": "extent",
              "field": "y1",
              "signal": "trellisExtent"
            }
          ]
        }
      ],
    
      "scales": [
        {
          "name": "xscale",
          "domain": {"data": "tuples", "field": "c"},
          "nice": true,
          "zero": true,
          "round": true,
          "range": "width"
        },
        {
          "name": "color",
          "type": "ordinal",
          "range": "category",
          "domain": {"data": "trellis", "field": "a"}
        }
      ],
    
      "axes": [
        { "orient": "bottom", "scale": "xscale", "domain": true }
      ],
    
      "marks": [
        {
          "type": "group",
    
          "from": {
            "data": "trellis",
            "facet": {
              "name": "faceted_tuples",
              "data": "tuples",
              "groupby": "a"
            }
          },
    
          "encode": {
            "enter": {
              "x": {"value": 0},
              "width": {"signal": "width"}
            },
            "update": {
              "y": {"field": "y0"},
              "y2": {"field": "y1"}
            }
          },
    
          "scales": [
            {
              "name": "yscale",
              "type": "band",
              "paddingInner": {"signal": "innerPadding"},
              "paddingOuter": {"signal": "outerPadding"},
              "round": true,
              "domain": {"data": "faceted_tuples", "field": "b"},
              "range": {"step": {"signal": "rangeStep"}}
            }
          ],
    
          "axes": [
            { "orient": "left", "scale": "yscale",
              "ticks": false, "domain": false, "labelPadding": 4 }
          ],
    
          "marks": [
            {
              "type": "rect",
              "from": {"data": "faceted_tuples"},
              "encode": {
                "enter": {
                  "x": {"value": 0},
                  "x2": {"scale": "xscale", "field": "c"},
                  "fill": {"scale": "color", "field": "a"},
                  "strokeWidth": {"value": 2}
                },
                "update": {
                  "y": {"scale": "yscale", "field": "b"},
                  "height": {"scale": "yscale", "band": 1},
                  "stroke": {"value": null},
                  "zindex": {"value": 0}
                },
                "hover": {
                  "stroke": {"value": "firebrick"},
                  "zindex": {"value": 1}
                }
              }
            }
          ]
        }
      ]
    }
    ```
  </div>
</details>

<details open="true">
  <summary>Show rendered plot</summary>
  <div markdown=1>
  ![Sample Vega diagram](https://kroki.hzdr.de/vega/svg/eNqtVzuP2zgQ7vdXCMQVl0DQyl5vEmx3xRVXXYArUgRGQEm0zFuJFEjKsdfQf8-QepEi5d0EaQxp-M17vhF9vYsi9IfMj6TG6ClCR6Ua-XR_fyIlTkqqjm2WUH7fA4z0_vSY_C85Q7FWLYjMBW0UBQGo_xUxIhUpogyLKD9ioSJyxnVTkTj6Dta0XEal4G2jQZcox4qUXFyS3tx3WqgjGHpIU_Pe4KKgrATJo3nHreKSvhDtC85ASUslLRmuJAi_wmsUXc0vHDBcG6jArCT_KdKgOEInXLVaujUuDC6jrADJFVHWtGrS0OiaMuMdnvBZP6XwKLWpp2jTGQNdHPZKGSPi85CB5ThNNm_3nE6e0-Tj7DpN0le881ateN_-mvfdz3g_EloeFZo8tU0BndYnSpCqovLvsyJMfd3sUW8Hfvd9NwGI11upWhgmORs2ac2tNzoID6FnWgPrdHJ4-pA8dPGrqF2yXUdls61P66h8RD0mGw-18Wztkp2H2nqoh-RxHZXPcW27AbSfaqSgq_LARe2WaXrSkEtjyovLUgDJoVWxfWwYm120fl-pDO0dwIGSqjBtgDDcI970cnwiAsNwuad4UpqkUwI3J2yYo3kSJG9FHpyR352_FWp825x22lamYiZRJBvMXNPk3AhnR0XvYUuyApA5-RPI0NZJzlum4sheKHFkE_wdenNIUuH8GfnNm6J7syViKLxi6rJxD_od7S0AtNb1eR_IHFfkxnI_G8Dc7YLX2Cyv67hKpoGw4svRlB5i1MyNEi2ZZC9E8KUMhsDsS1doNubT-O26ObM5r7iw5nIoJBfQQyiOb3P8PN7ObmSClR5G3bKO-GxXUXulugF6x3CleK31-1JaRbUc6qxtezUWz4G2jEkZyvQf6H4yBNf8myfJj38-OsDsKwdtVdEckuLbguauzcDZTGIojzd33RwqYTkviBsslIqIZURn04fpy9o57sa7zNWa_V7WhSg2fyIdFxdjYaZVilwvl-0CsLHN-7kt2OSReyzyxWWV01q9ntyT4Z72j15Qi5SdW1AX0vq3VZ6Wc3txtUIsXKPGclQshmRLswPnruMlxw5nvkB2fnX3c3Udjnk8q8hBOSwLFpnmhlYHuNESm3-joMIZqT5PF-NdKA6Xm6sbXJB8ub8Hlq5V0K1ZgCirZHmVMD2gH-fAHgptbuuzU1Wu4rBq3ZXo6Ukl-DP5MjF1-m_QOciFYpiqFl29Bq_PndEabssrqoZu-t_GSvhO5KytKh_4Ahd9siz-zRSP_BRuYcAnJCdIJmBw0Ztcbxau70LPgfvAXfcD8eeNnA==)
  </div>
</details>

Taken from <https://vega.github.io/vega/examples/nested-bar-chart/>.

### C4 with PlantUML

<details>
  <summary>Show code</summary>
  <div markdown="1">  
    ```plantuml
    @startuml "techtribesjs"
    !include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
    ' uncomment the following line and comment the first to use locally
    ' !include C4_Container.puml
    
    LAYOUT_TOP_DOWN()
    'LAYOUT_AS_SKETCH()
    LAYOUT_WITH_LEGEND()
    
    
    Person_Ext(anonymous_user, "Anonymous User")
    Person(aggregated_user, "Aggregated User")
    Person(administration_user, "Administration User")
    
    System_Boundary(c1, "techtribes.js"){
        
        Container(web_app, "Web Application", "Java, Spring MVC, Tomcat 7.x", "Allows users to view people, tribes, content, events, jobs, etc. from the local     tech, digital and IT sector")
    
        ContainerDb(rel_db, "Relational Database", "MySQL 5.5.x", "Stores people, tribes, tribe membership, talks, events, jobs, badges, GitHub repos, etc.")
    
        Container(filesystem, "File System", "FAT32", "Stores search indexes")
    
        ContainerDb(nosql, "NoSQL Data Store", "MongoDB 2.2.x", "Stores from RSS/Atom feeds (blog posts) and tweets")
    
        Container(updater, "Updater", "Java 7 Console App", "Updates profiles, tweets, GitHub repos and content on a scheduled basis")
    }
    
    System_Ext(twitter, "Twitter")
    System_Ext(github, "GitHub")
    System_Ext(blogs, "Blogs")
    
    
    Rel(anonymous_user, web_app, "Uses", "HTTPS")
    Rel(aggregated_user, web_app, "Uses", "HTTPS")
    Rel(administration_user, web_app, "Uses", "HTTPS")
    
    Rel(web_app, rel_db, "Reads from and writes to", "SQL/JDBC, port 3306")
    Rel(web_app, filesystem, "Reads from")
    Rel(web_app, nosql, "Reads from", "MongoDB wire protocol, port 27017")
    
    Rel_U(updater, rel_db, "Reads from and writes data to", "SQL/JDBC, port 3306")
    Rel_U(updater, filesystem, "Writes to")
    Rel_U(updater, nosql, "Reads from and writes to", "MongoDB wire protocol, port 27017")
    
    Rel(updater, twitter, "Gets profile information and tweets from", "HTTPS")
    Rel(updater, github, "Gets information about public code repositories from", "HTTPS")
    Rel(updater, blogs, "Gets content using RSS and Atom feeds from", "HTTP")
    
    Lay_R(rel_db, filesystem)
    
    @enduml
    ```
  </div>
</details>

<details open="true">
  <summary>Show rendered plot</summary>
  <div markdown=1>
  ![Sample C4 PlantUML diagram](https://kroki.hzdr.de/plantuml/svg/eNqNVU1z2zgMvetXYH2pMqPabbJtZvZUJ06Tdp2PRvJmetJQEmyzpUgtScXx7Ox_L0jJshS3k_pgUeAj8AA8UB-MZdrWpYCRxXxtNc_QfDOj4A8uc1EXCGtrK_PXZKLZZrzidl1ntUGdK2lR2nGuykklmHQuXhtbCJ5Nzv98fedMi-v5pGTGoiZTek4nGJeoxxVhg1dQSzpckhOwa4SlEkJtuFyBIBAwWcBgm2tDKwUUHITKmRBb8tGxPAwQzKdfbxdJmtzepbPbh5vwKHjVmqZxGv99kZxfka01PXxKrtL5xeXFzYyMQXCH2iiZXjzZkEklt6WqTeoSj2A03RlgQYbRUQsO2WqlccUsFh2yszyHFiWX3FjNLKcwO_jAujsSxFuqYZmeqVoWTG_D_G3U79aY2nX0XwD0839dHcINZimrKkI_YAbTqhI8965HZPrMHlkEcaVdza__OY8gUSVtw-n4ye1PXT-Mq7c2rvCPHDdQoaoERtBEjqCVQQT4SA8yfFMZ_aPNx7DUqvS98-0CRziCgpOG6M3191MCBnOrfJID5rMs1CjSIiMe9yg8Zzo0Y5ZlzKCjd72Nv8zh3fhdwzYmN2gO-PknlFhmlMWaUyko-HfznG_GipWDX3J7VWegsVJtFgfUwiUXaHxHKOxHeoGmP47Fx2lyctyjY5DpfA1cFviE5mdpSmX-FXTgRrl0XILgz_oUlVyp2Rkcj48HSfrC3sfxZGppsUQsDISZUCsg2tYc-eLaDaI9DBnWVUF6dGpbNKudFuDUoYyihEgpow5ARdXKJx21Tod1akfV6wBItQxMvsaiFiR6ahZ3HP7vROwGym64bSgkzYoQve3mkqHdJspw06VJBEZn7umyC0gfByO6Fz7NkHG5XCXJXUx4j34-py_Afzarvz7iz3TbPRmzom2dK9hGc1daq3xfv8wnn2dnNIKV0hZOTt68b2N3fgaq2_t6DtvJqYfoCWnDNbpuWpUr0QY7Pn3z9rSlnS728niBeOGU-gL7vrsB_4cu-QPcYQKH5frddPZe94q7JP3uBE1zuVS6bC7b_cx0ZeuLoHO1V6eDDjxkqrZQ1RndsjQQ9FHy88FpaDm-4HUna-90N0y1cVczDbon1xv2viuX65xt0_vuxtyXmrY-oCzoa_gDr6egyQ==)
  </div>
</details>

Taken from <https://github.com/plantuml-stdlib/C4-PlantUML>.

### GraphViz

<details>
  <summary>Show code</summary>
    <div markdown="1">
    {% raw %}
    ```graphviz
    digraph G {
      concentrate=True;
      rankdir=TB;
      node [shape=record];
      140087530674552 [label="title: InputLayer\n|{input:|output:}|{{[(?, ?)]}|{[(?    , ?)]}}"];
      140087537895856 [label="body: InputLayer\n|{input:|output:}|{{[(?, ?)]}|{[(?,     ?)]}}"];
      140087531105640 [label="embedding_2: Embedding\n|{input:|output:}|{{(?, ?)}|{    (?, ?, 64)}}"];
      140087530711024 [label="embedding_3: Embedding\n|{input:|output:}|{{(?, ?)}|{    (?, ?, 64)}}"];
      140087537980360 [label="lstm_2: LSTM\n|{input:|output:}|{{(?, ?, 64)}|{(?,     128)}}"];
      140087531256464 [label="lstm_3: LSTM\n|{input:|output:}|{{(?, ?, 64)}|{(?, 32)    }}"];
      140087531106200 [label="tags: InputLayer\n|{input:|output:}|{{[(?, 12)]}|{[(?    , 12)]}}"];
      140087530348048 [label="concatenate_1: Concatenate\n|{input:|output:}|{{[(?,     128), (?, 32), (?, 12)]}|{(?, 172)}}"];
      140087530347992 [label="priority: Dense\n|{input:|output:}|{{(?, 172)}|{(?, 1)    }}"];
      140087530711304 [label="department: Dense\n|{input:|output:}|{{(?, 172)}|{(?,     4)}}"];
      140087530674552 -> 140087531105640;
      140087537895856 -> 140087530711024;
      140087531105640 -> 140087537980360;
      140087530711024 -> 140087531256464;
      140087537980360 -> 140087530348048;
      140087531256464 -> 140087530348048;
      140087531106200 -> 140087530348048;
      140087530348048 -> 140087530347992;
      140087530348048 -> 140087530711304;
    }
    ```
    {%endraw%}
  </div>
</details>

<details open="true">
  <summary>Show rendered plot</summary>
  <div markdown=1>
  ![Sample GraphViz diagram](https://kroki.hzdr.de/graphviz/svg/eNqtk01rwkAQhu_-isVTAxY2m69NJBX6QSnYU71ZkdVdNDRuwmY9SPS_d2NiEnWrFTyEvBOGZyYz79BoIUi6BO8g7wAwT_iccSmIZOFIrFlffROE_9BIhKPnIuIJZWCcLUnKQsHmiaCT4rNpQ4g9x4KuZzsOAuOYzFgcdmUkYxaAD56u5ZBsmPjm2zwqomCbrGXx3m3zfPww6IGBMVG6lrvuEdnDvoMdtybPErq5C9g0oePasAaz1YxRGvHFFAXg7RDo8SVSqb3oAdc2TvHQUwWQrcFb98B7PoaW23QfZ3JVND78Gn1egJasEmwifIY1kZqJax9jrZuwFjI0o3YRbJqVZJH9c4cmapa416djtmwMbVyjCycrF3P1TM0AvDThxSLY6IGq-VJUdffSO_8jVdbz_cbvqYgSEUnlzFfGM_b3qPawiqv1jAWb6VOWEiFX6jJv4Wq8WF3n49Op-3WX1sqqTKw7m1ZW5UWd-9sVS2vpTNyuWO5T58krWZXHLmcd7HKcVazyWla5mn5n9wvGN3i8)
  </div>
</details>

Taken from <https://graphviz.org/Gallery/directed/neural-network.html>.

### BlockDiag

<details>
  <summary>Show code</summary>
  <div markdown="1">  
    ```blockdiag
    blockdiag {
      A -> B -> C -> D;
      A -> E -> F -> G;
    }
    ```
  </div>
</details>

<details open="true">
  <summary>Show rendered plot</summary>
  <div markdown=1>
  ![Sample Blockdiag diagram](https://kroki.hzdr.de/blockdiag/svg/eNpLyslPzk7JTExXqOZSUFBwVNC1U3ACEc4gwsUaLugKItxAhLs1Vy0Ae3UMLA==)
  </div>
</details>

Taken from <http://blockdiag.com/en/blockdiag/examples.html>.

## Deployment
The service is provided by HIFIS Software and is hosted entirely on HZDR
servers.
It does not share any data with external providers.
As usual, the deployment can be reproduced via the corresponding
[project](https://gitlab.hzdr.de/hifis-software-deployment/kroki) - 
only accessible for employees within Helmholtz.

## Comments and Suggestions

If you have suggestions, questions, or queries, please don't hesitate to write us.

<a href="{% link contact.md %}" 
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk"> 
                            Contact us! <i class="fas fa-envelope"></i></a>
