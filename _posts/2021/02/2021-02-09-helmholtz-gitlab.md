---
title: "Helmholtz GitLab Available"
title_image: clark-tibbs-oqStl2L5oxI-unsplash.jpg
date: 2021-02-09
authors:
  - huste
layout: blogpost
categories:
  - News
excerpt: >
    The HIFIS Software team is happy to announce the general availability
    of the Helmholtz GitLab which can be used free-of-charge by anybody
    within Helmholtz and their partners. It is hosted by HIFIS Software 
    entirely on HZDR servers.

---

# Helmholtz GitLab available

The HIFIS Software team is happy to announce the general availability of the
[Helmholtz GitLab](https://gitlab.hzdr.de) which can be used free-of-charge
by anybody within Helmholtz and their partners.
It is hosted by HIFIS Software entirely on HZDR servers, jointly created by
UFZ and HZDR.  
With the general availability of the Helmholtz Cloud, the service will be
available as a fully integrated Hemholtz Cloud Service and will follow the
given regulations.

## How to access?

Visit [https://gitlab.hzdr.de](https://gitlab.hzdr.de) and login via the
[_Helmholtz AAI_]({% post_url 2020/07/2020-07-24-helmholtz-aai %}).
Your account will automatically be provisioned.

## What do I get?

The service is running the Open Source version of GitLab. It allows you to

* Host code in repositories with Git version control,
* Implement project management with an [Issue Tracker][issue-tracker],
  [Merge Requests][merge-requests] or [Issue Boards][issue-boards],
* Use the provided shared runners for built-in [Continuous Integration][ci],
* Host your Docker images using the [Container Registry][registry],
* Share your documentation or static websites with [Pages][pages] or
* Share private or public software [packages][packages] for a variety of common
  package managers, e.g. [Conan][conan] (C++) or [PyPI][pypi] (Python).

<i class="far fa-question-circle"></i> Want to get started with Git and GitLab? [Follow this link][new-to-git] for a
quick-start introduction.

## Technical Details

We aim to be as open and transparent as possible while hosting the service.
Therefore, we decided to manage the service via Ansible and to make the source code
generally available. Thus, we allow you to reproduce, validate and understand
the deployment of the service via [this project][ansible-deployment] together
with the published [Ansible roles][roles].

<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> In-depth Software Consulting</h2>
  <p>
    HIFIS Software offers free-of-charge workshops and consulting to research groups within the Helmholtz umbrella.
    You can read more about what we offer on our
    <strong><a href="{% link services/index.md %}">services page</a></strong>.
    If you work within the Helmholtz Association, and think that something like this would be useful to you,
    fill in our
    <strong><a href="{% link services/software/consulting.html %}#consultation-request-form">consultation request form</a></strong>.
  </p>
</div>


[issue-tracker]: https://gitlab.hzdr.de/help/user/project/issues/index.md#issues-list
[merge-requests]: https://gitlab.hzdr.de/help/user/project/merge_requests/index.md
[issue-boards]: https://gitlab.hzdr.de/help/user/project/issues/index.md#issue-boards
[ci]: https://gitlab.hzdr.de/help/ci/index.md
[registry]: https://gitlab.hzdr.de/help/user/packages/container_registry/index.md
[pages]: https://gitlab.hzdr.de/help/user/project/pages/index.md
[packages]: https://gitlab.hzdr.de/help/user/packages/package_registry/index.md
[conan]: https://gitlab.hzdr.de/help/user/packages/conan_repository/index.md
[pypi]: https://gitlab.hzdr.de/help/user/packages/pypi_repository/index.md
[new-to-git]: https://gitlab.hzdr.de/help/#new-to-git-and-gitlab
[ansible-deployment]: https://gitlab.hzdr.de/hifis-software-deployment/gitlab
[roles]: https://gitlab.com/hifis/ansible
