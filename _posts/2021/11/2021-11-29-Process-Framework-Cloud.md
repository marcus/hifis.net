---
title: "Process Framework for Helmholtz Cloud Service Portfolio"
title_image: background-cloud.jpg
date: 2021-11-29
authors:
  - holz
  - spicker
layout: blogpost
categories:
  - Milestone
excerpt: >
  The Process Framework for Helmholtz Cloud Service Portfolio is now available in version 1.0! 

---

### Process Framework for Helmholtz Cloud Service Portfolio
The [Process Framework for Helmholtz Cloud Service Portfolio](https://hifis.net/doc/process-framework/Chapter-Overview/) is now available in version 1.0! It focuses on the explanation of the processes regarding the Service Portfolio Management for Helmholtz Cloud, giving an overview of which processes exist, how they interconnect, which roles are involved in each process and what is included in each process step. 

This Process Framework serves as a guide to understand how Service Portfolio Management for Helmholtz Cloud works.

#### Information about Helmholtz Cloud
In the Helmholtz Cloud, members of the Helmholtz Association of German Research Centres provide selected IT-Services for joint use.
The Service Portfolio covers the whole scientific process and offers Helmholtz employees and their project partners a federated community cloud with uniform access for them to conduct and support excellent science. [Read more!]({% link services/cloud/Helmholtz_cloud.md %})

#### Comments and Suggestions

If you have suggestions or questions, please do not hesitate to contact us.

<a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>


