---
title: "GitLab for Software Development in Teams"
layout: event
organizers:
  - schlauch
lecturers:
  - schlauch
  - "1 &times; HZDR"
  - Leinweber, Katrin
type:   workshop
start:
    date:   "2020-06-09"
    time:   "09:00"
end:
    date:   "2020-06-10"
    time:   "17:00"
location:
    campus: "Online"
#    room:   "TBD"
fully_booked_out: true
registration_link: https://events.hifis.net/event/13/registrations
waiting_list_link: https://events.hifis.net/event/15/registrations/11/
registration_period:
    from:   "2020-02-20"
    to:     "2020-05-30"
excerpt: 
    "Using GitLab's collaboration and automation features effectively"
---

### Goal

Help developers of medium and large software systems to use the
collaboration and automation features of GitLab in order to
improve sustainability in their projects.

## Content

This workshop teaches you how to use
basic and advanced GitLab features to
collaborate with others on a software project.

You will learn how to:
* Effectively plan and track work using issues
* Review change suggestions with merge requests
* Create and document releases, and
* How to automate repetitive tasks with continuous integration pipelines.

We will complete a few simulated iterations
of the software development lifecycle in a demo project,
both alone and in a team.

### Prerequisites

- Basic Git skills are needed. A good and quick tutorial can be found in the 
[Software Carpentry's "Git Novice" episodes 1 to 9](https://swcarpentry.github.io/git-novice/).
- You require your laptop, plus the Git command line or a graphical client, a modern web browser, and a text editor.
