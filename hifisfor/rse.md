---
title: HIFIS for Research Software Engineers
title_image: adi-goldstein-mDinBvq1Sfg-unsplash_shrinked.jpg
layout: services/default
additional_css:
  - image-scaling.css
excerpt: >-
    No text.
---

**HIFIS supports Research Software Engineering** with Education & Training, Consulting, Platform and Community Services.

<div class="floating-boxes">
    <div class="image-box align-left">
    <img
        class="left medium nonuniform"
        alt="Poster Element 1: We are starting a scientific Project. Can you help us?"
        src="{{ site.directory.images | relative_url }}/illustrations/undraw_teacher_re_sico.svg"
    />
    </div>
    <div class="text-box">
        <h3>Education & Training</h3>
        <p markdown=1>
        We offer courses, materials, and workshops to help you in getting started, or to boost your engineering practices.
        Check out the
        [HIFIS Events page](https://events.hifis.net/category/4/) or the
        [HIDA course catalog](https://www.helmholtz-hida.de/course-catalog/en/) for upcoming HIFIS courses and events.
        [Course material](https://gitlab.com/hifis/hifis-workshops) is available for use and reuse under a Creative Commons License.
        </p>
        <p markdown=1>
        Can’t find exactly what you’re looking for? [Talk to us directly](mailto:support@hifis.net).
        </p>
    </div>
</div>
<div class="clear"></div>

<div class="floating-boxes">
    <div class="image-box align-right">
    <img
        class="right medium nonuniform"
        alt="Two people standing either side of a board containing goals to be completed"
        src="{{ site.directory.images | relative_url }}/services/undraw_team_goals_hrii.svg"
    />
    </div>
    <div class="text-box">
        <h3>Consulting</h3>
        <p markdown=1>
        HIFIS offers free-of-charge consulting as a service to research groups under the Helmholtz umbrella.
        We can help you to deal with specific licensing issues, pointing out solutions on how
        to improve your software or setting up a new projects.
        We are also very happy to discuss other software engineering topics such as software engineering process in general.
        We kindly ask you to fill out a few questions to request a consultation.
        [Request a consultation now!]({% link services/software/consulting.html %})
        </p>
    </div>
</div>
<div class="clear"></div>

<div class="floating-boxes">
    <div class="image-box align-left">
    <img
        class="left medium nonuniform"
        alt="A software developer working with GitLab."
        src="{{ site.directory.images | relative_url }}/services/undraw_developer_activity_bv83.svg"
    />
    </div>
    <div class="text-box">
        <h3>Platform and Cloud Services for Research Software Development</h3>
        <p markdown=1>
        We provide a sustainable, well integrated, easy-to-use Technology Infrastructure and Cloud Service Portfolio for scientific software. For an overview, have a look at our [Cloud Services Portal](https://helmholtz.cloud/services).</p><p markdown=1>
        We offer the [Helmholtz Codebase](https://helmholtz.cloud/services?serviceDetails=svc-c5d1516e-ffd2-42ae-b777-e891673fcb32) to all Helmholtz, disregarding the centre affiliation.
        A feature overview is available [here]({% link services/software/helmholtzgitlab.html %}).
        </p>
        <p markdown=1>
        In addition, a [chat service](https://helmholtz.cloud/services?serviceDetails=svc-1be91786-b7e7-4fa3-81d9-1b95dd03cd52) to foster team communication is available for all of Helmholtz.
        </p>
    </div>
</div>
<div class="clear"></div>

<div class="floating-boxes">
    <div class="image-box align-right">
    <img
        class="right medium nonuniform"
        alt="Poster Element 4: Data Re-use. We help you to transfer your results."
        src="{{ site.directory.images | relative_url }}/services/community_rsd.svg"
    />
    </div>
    <div class="text-box">
        <h3>Communities</h3>
        <p markdown=1>
        We build and foster communities to support the necessary cultural change in science when dealing with research software.
        We have plans to establish a Helmholtz-wide Research Software Directory (RSD) to support the findability and reusability
        of research software.
        </p>
        <p markdown=1>
        In addition, we implement the [Helmholtz Software Spotlights]({% link spotlights.md %})
        as the public face of the Helmholtz Network for Research Software Engineering.
        The collection is intended to become a "flagship" of the Helmholtz Association
        for research software that appeals equally to research, citizens, industry and politics.
        </p>
        <p markdown=1>
        All details are available on the [Community page]({% link services/software/communities.html %}).
        </p>
    </div>
</div>
<div class="clear"></div>

<div class="floating-boxes">
    <div class="image-box align-left">
    <img
        class="left medium nonuniform"
        alt="HIFIS Logo, claim. Contact us anytime at support@hifis.net"
        src="{{ site.directory.images | relative_url }}/hifisfor/HIFIS_logo_claim_blue.svg"
    />
    </div>
    <div class="text-box">
        <h3>Queries, comments and suggestions</h3>
        <p>
            If you have suggestions or questions, please do not hesitate to contact us.
        </p>
        <p>
            <a href="{% link contact.md %}"
                            class="btn btn-outline-secondary"
                            title="HIFIS Helpdesk">
                            Contact us! <i class="fas fa-envelope"></i></a>
        </p>
    </div>
</div>
<div class="clear"></div>

<div class="image-attrib">Images by Katerina Limpitsouni via <a href="https://undraw.co/">undraw.co</a></div>
