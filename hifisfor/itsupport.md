---
title: HIFIS for IT Support Experts
title_image: default
layout: default
additional_css:
  - image-scaling.css
excerpt: >-
    HIFIS for IT Support Staff.
---

If you **work in local IT support in a Helmholtz Centre**, find information here on how HIFIS can assist you to give advise to your clients.

## What HIFIS can do for IT supporters

<div class="floating-boxes">
    <div class="image-box align-right">
    <img
        class="right medium nonuniform"
        alt="Symbolic Picture 1"
        src="{{ site.directory.images | relative_url }}/illustrations/undraw_software_engineer_lvl5.svg"
    />
    </div>
    <div class="text-box">
        <ol>
        <li>
        <p>We provide a <strong>multitude of cloud services</strong> for Helmholtz.
        That is, several services are already provided ready-to-use by selected centres for the Helmholtz Association, others are bound to specific usage conditions, and further services may be in the pipeline or can be <a href="mailto:support@hifis.net">requested</a>.</p>
        <p>The provided services comprise collaboration tools, compute and storage services, and more.
        See this <a href="{{ "services/cloud/Helmholtz_cloud" | relative_url }}">overview for general explanations</a>, and, most importantly, the <a href="https://helmholtz.cloud/services"><strong>Helmholtz Cloud Portal</strong></a> for a full overview on services, service descriptions and usage conditions.</p>
        </li>
        </ol>
    </div>
</div>
<div class="clear"></div>

<div class="floating-boxes">
    <div class="image-box align-left">
    <img
        class="left medium nonuniform"
        alt="Symbolic Picture 2"
        src="{{ site.directory.images | relative_url }}/illustrations/undraw_teacher_re_sico.svg"
    />
    </div>
    <div class="text-box">
        Further check our information and documentation:
        <ul>
        <li><a href="https://hifis.net/doc"><strong>Technical and Administrative Documentation for HIFIS Services</strong></a></li>
        <li><a href="https://hifis.net/doc/service-portfolio/service-portfolio-management/current-services-in-portfolio/">Cloud Services that are scheduled to onboard soon (service pipeline)</a></li>
        <li><a href="{{ "services/cloud/provider" | relative_url }}">Steps to take for handing in new Cloud Services</a></li>
        <li><a href="https://aai.helmholtz.de/tutorial/2021/06/23/how-to-helmholtz-aai.html">Illustrated tutorial on how to use AAI <i class="fas fa-external-link-alt"></i></a> &mdash; the central Authentication and Authorisation Infrastructure &mdash; to log in to services</li>
        <li><a href="https://hifis.net/doc/helmholtz-aai/howto-vos/">How to register a service usage group</a> &mdash; a so-called virtual organisation (VO)</li>
        <li><a href="https://hifis.net/doc/helmholtz-aai/howto-vo-management/">How to manage such service usage groups</a></li>
        </ul>
    </div>
</div>
<div class="clear"></div>

<div class="floating-boxes">
    <div class="image-box align-right">
    <img
        class="right medium nonuniform"
        alt="Symbolic Picture 3"
        src="{{ site.directory.images | relative_url }}/illustrations/undraw_team_spirit_hrr4.svg"
    />
    </div>
    <div class="text-box">
        <ol start="2">
        <li>
        <p>We provide <a href="{{ "services/software/training" | relative_url }}"><strong>training and courses</strong></a> on techniques for research software engineering, e.g. on programming languages, best practices, distributed service usage.</p>
        </li>
        <li>
        <p>We give <a href="{{ "consulting" | relative_url }}"><strong>consulting</strong></a> on complex questions related to software projects.</p>
        </li>
        <li>
        <p>We provide <a href="{{ "services/overall/learning-materials" | relative_url }}"><strong>learning materials and guidelines</strong></a>, e.g. on (Research) Software Engineering.</p>
        </li>
        </ol><p>For any questions, queries and comments on any of the abovementioned points and services,
        you and your users are invited to 
        <a href="{{ "contact" | relative_url }}"><strong>contact us anytime via our contact form</strong></a> or equivalently via <a href="mailto:support@hifis.net"><strong>support@hifis.net</strong></a>.</p>
    </div>
</div>
<div class="clear"></div>

---

## What we can not do

<div class="floating-boxes">
    <div class="image-box align-left">
    <img
        class="left medium nonuniform"
        alt="Symbolic Picture 4"
        src="{{ site.directory.images | relative_url }}/illustrations/undraw_cancel_re_pkdm.svg"
    />
    </div>
    <div class="text-box">
        <p>
        In general, we are <i>not</i> entitled to assist with any problems or issues that can and should be solved locally, i.e. within one centre or institute.
        For example:
        <ul>
        <li>Local software problems or local network problems,</li>
        <li>Any issues arising in your local computing centre,</li>
        <li>Information on IT problems, software, codes, etc. that are not in our portfolio,</li>
        <li>Issues and questions about internal processes and regulations, and</li>
        <li>Comparable issues...</li>
        </ul>
        </p>
    </div>
</div>
<div class="clear"></div>
