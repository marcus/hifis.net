---
title: Software Development Infrastructure
title_image: adi-goldstein-mDinBvq1Sfg-unsplash_shrinked.jpg
layout: services/default
author: none
additional_css:
    - services/services-page-images.css
excerpt: >-
    We provide a sustainable, well integrated and easy to
    use technology infrastructure for scientific software.
---

<p>
    The HIFIS team offers IT services to all Helmholtz communities
    which enable scientists to do research and develop research software in
    tight collaboration with other scientists from within Helmholtz.
    Offering a sophisticated Software Project Management Platform is an
    important prerequisite to achieve this.
    The Software Project Management Platform
    <a href="https://about.gitlab.com">GitLab</a>
    has already become a de-facto standard for many Helmholtz centers
    in the past years.
    Many centers offer GitLab to their scientists.
    In order to bundle resources and foster cross-center collaboration
    HIFIS Software offers
    <a href="https://gitlab.hzdr.de">Helmholtz GitLab</a>
    to all Helmholtz disregarding the center affiliation.
    The benefits and synergies of using GitLab are manifold.
    Nowadays, research software needs to be open, transparent, reproducible,
    accessible, findable, reusable just to name a few desired properties.
    We believe that a full-fledged feature-rich open-source software like
    GitLab is the right tool to support scientists to develop such software
    that adheres to these properties.
</p>

<h1>Features of the Helmholtz GitLab Service</h1>

<div class="image-block">
    <img
        class="help-image left"
        alt="A software developer working with version control."
        src="{{ site.directory.images | relative_url }}/services/undraw_version_control_9bpv.svg"
    />
    <div>
        <h3>Version Control with Git</h3>
        <p>
            One reason for many Helmholtz research centres to have chosen GitLab as
            the right tool might be the popularity of the Version Control System
            <a href="https://git-scm.com/">Git</a>.
            GitLab is using Git under the hood and Git and GitLab interact
            with each other seamlessly.
            With Version Control scientists are able to keep track of their
            changes they make during research software development due to the
            fact that Git stores the complete history of changes in the
            respective Git repository.
            One can jump back and forth in the history and for example inspect
            previous versions or even restore previous versions of the software.
            GitLab or any other Git-based Software Project Management Platform
            offer a central point to exchange code and ideas with others and
            render close and fruitful collaboration between scientists.
            But what if you already use another Git-based Software
            Project Management Platform?
            That's perfect, no worries.
            GitLab offers an
            <a href="https://gitlab.hzdr.de/help/user/project/import/index.md">import wizard</a>
            with which researchers can migrate their remote Git repositories
            just easily if the necessity arises.
        </p>
        <p>
            <a href="https://gitlab.hzdr.de/help/topics/git/index.md" type="button" class="btn btn-outline-secondary btn-sm">
                <i class="fas fa-file-alt"></i> Documentation
            </a>
        </p>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image right"
        alt="A software developer working with GitLab."
        src="{{ site.directory.images | relative_url }}/services/undraw_developer_activity_bv83.svg"
    />
    <div>
        <h3>Project Management with GitLab</h3>
        <p>
            As mentioned in the previous section, collaboration is such a necessity.
            Today, there is literally no research project that is not driven
            by collaboration.
            Hence, project management is an important task and duty of
            researchers in order to make good progress in their research
            projects.
            GitLab is a specialized tool to support software project management.
            It offers
            <a href="https://gitlab.hzdr.de/help/user/group/index.md">Groups</a>,
            <a href="https://gitlab.hzdr.de/help/user/project/index.md">Projects</a>,
            <a href="https://gitlab.hzdr.de/help/user/project/issues/index.md">Issue Trackers</a>,
            <a href="https://gitlab.hzdr.de/help/user/project/issue_board.md">Issue/Kanban Boards</a>,
            <a href="https://gitlab.hzdr.de/help/user/project/milestones/index.md">Milestones</a>,
            <a href="https://gitlab.hzdr.de/help/user/project/labels.md">Labels</a>,
            <a href="https://gitlab.hzdr.de/help/user/project/repository/branches/index.md">Branches</a>,
            <a href="https://gitlab.hzdr.de/help/user/project/merge_requests/index.md">Merge Requests</a>
            among many other useful features.
            This platform is very open in terms of supported workflows in
            research software projects and can be adapted to almost any
            practical software development workflow you may think of.
            Using the <a href="https://gitlab.hzdr.de">Helmholtz GitLab Service</a>
            makes it easier to see and keep track of the progress of your projects.
        </p>
        <p>
            <a href="https://gitlab.hzdr.de/help/user/project/index.md" type="button" class="btn btn-outline-secondary btn-sm">
                <i class="fas fa-file-alt"></i> Documentation
            </a>
        </p>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image left"
        alt="A software developer watching Continuous Integration pipeline stages."
        src="{{ site.directory.images | relative_url }}/services/undraw_check_boxes_m3d0.svg"
    />
    <div>
        <h3>GitLab Continuous Integration</h3>
        <p>
            Continuous Integration (CI) is a first-class citizen available free-of-charge
            and already activated for all users.
            Almost every software product benefits from automated tasks.
            CI pipelines can automatically build, test and finally deploy the
            software product to a container, package registry or a staging/production
            environment on which simulations may run.
            Often these automated task are used as quality gates which
            need to be passed successfully in order to be able to integrate
            the code change into some mainline.
            Developers are able to "integrate" their contributions
            "continuously" and due to a high level of automation many of the time
            consuming tasks don't need to be done manually.
            Thus, researchers can focus on doing research and not on doing
            tedious tasks again and again.
        </p>
        <p>
            <a href="https://gitlab.hzdr.de/help/ci/index.md" type="button" class="btn btn-outline-secondary btn-sm">
                <i class="fas fa-file-alt"></i> Documentation
            </a>
        </p>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image right"
        alt="A container ship carrying some containers."
        src="{{ site.directory.images | relative_url }}/services/undraw_Container_ship_ok1c.svg"
    />
    <div>
        <h3>GitLab Container Registry</h3>
        <p>
            Containers have become very popular: Gitlab takes that into account
            by offering Container Registry very similar to Docker Hub.
            Once research software developers build their own images they can
            be stored centrally in such a registry for later reuse.
        </p>
        <p>
            <a href="https://gitlab.hzdr.de/help/user/packages/container_registry/index.md" type="button" class="btn btn-outline-secondary btn-sm">
                <i class="fas fa-file-alt"></i> Documentation
            </a>
        </p>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image left"
        alt="A software developer inspecting some packages."
        src="{{ site.directory.images | relative_url }}/services/undraw_order_delivered_p6ba.svg"
    />
    <div>
        <h3>GitLab Package Registry</h3>
        <p>
            Package Registries exist already for a long time, GitLab supports
            the most prominent candidates like PyPI (Python), Conan (C++),
            Maven (Java), NPM (NodeJS), Composer (PHP), NuGet (.NET), etc.
            so that packages can be stored independently from these public
            registries. On top GitLab also adds access control to
            keep the group of project members manageable.
        </p>
        <p>
            <a href="https://gitlab.hzdr.de/help/user/packages/package_registry/index.md" type="button" class="btn btn-outline-secondary btn-sm">
                <i class="fas fa-file-alt"></i> Documentation
            </a>
        </p>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image right"
        alt="A software developer with a bookshelf with some books."
        src="{{ site.directory.images | relative_url }}/services/undraw_Bookshelves_re_lxoy.svg"
    />
    <div>
        <h3>GitLab Pages</h3>
        <p>
            Additionally, there is a feature called GitLab Pages that can be
            used to publish static web sites e.g. your documentation.
            Like other features, e.g. GitLab Container andd Package Registries,
            GitLab Pages can be used during automation with CI pipelines.
            Each time a contribution is made to your project
            your documentation is rebuilt including the new changes
            and automatically made accessible to anyone with the right permissions.
        </p>
        <p>
            <a href="https://gitlab.hzdr.de/help/user/project/pages/index.md" type="button" class="btn btn-outline-secondary btn-sm">
                <i class="fas fa-file-alt"></i> Documentation
            </a>
        </p>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image left"
        alt="A helpdesk agent at his/her desk."
        src="{{ site.directory.images | relative_url }}/services/undraw_feeling_proud_qne1.svg"
    />
    <div>
        <h3>Help and Support</h3>
        <p>
            Please contact us regarding questions, issues, bugs or ideas
            on how to improve the service via the HIFIS Helpdesk.

            {% include contact_us/mini.html contact_mail_link=site.contact_mail_for_gitlab %}
            {% include helpdesk/mini.html %}

        </p>
    </div>
</div>

<div class="image-attrib">Images by Katerina Limpitsouni via <a href="https://undraw.co/">undraw.co</a></div>
