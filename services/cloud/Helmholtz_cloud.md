---
title: Helmholtz Cloud 
title_image: background-cloud.jpg
layout: services/default
author: none
additional_css:
    - services/services-page-images.css
redirect_from:
    - services/cloud-overview
    - services/cloud/Service_portfolio
    - services/cloud/cloud_login
excerpt: >-
    The Helmholtz Cloud offers IT services to all Helmholtz centres and the entire scientific community and partners.
---

<div class="image-block">
    <img
        class="help-image left"
        alt="Helmholtz Cloud Portal Screenshot"
        src="{{ site.directory.images | relative_url }}/services/Cloud_Portal.jpg"
        style="max-width: 20rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <p>
            <strong>In the <a href="https://helmholtz.cloud/">Helmholtz Cloud</a>, members of the Helmholtz Association of German Research Centres provide selected IT-Services for joint use.</strong><br>
        The Service Portfolio covers the whole scientific process and offers Helmholtz employees and their project partners a federated community cloud with uniform access for them to conduct and support excellent science. 
        The connection to international developments (e.g. EOSC at European level) is regarded as essential and the "FAIR" principles in particular are taken into account.
        </p>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image right"
        alt="Helmholtz Cloud Portal"
        src="{{ site.directory.images | relative_url }}/services/Cloud_Portal_Services.png"
        style="max-width: 20rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <h3>Helmholtz Cloud Offerings</h3>
        <p>
            Formerly isolated applications converge to central collaboration services for science as well as infrastructure, offered in the Helmholtz Cloud. Currently, the Helmholtz Cloud portfolio includes 22 services. The complete list and all further information can be found at the Helmholtz Cloud Portal. A selection:
        </p>
            <ul>
                <li> <a href="https://helmholtz.cloud/services?serviceDetails=svc-ec78dddd-f44b-4062-a1a1-ba9c0ddaa70b">HIFIS Events</a> – Managing events for everyone </li>
                <li> <a href="https://helmholtz.cloud/services?serviceDetails=svc-1be91786-b7e7-4fa3-81d9-1b95dd03cd52">Mattermost</a> – Web based chat service </li>
                <li> <a href="https://helmholtz.cloud/services?serviceDetails=svc-cea6f848-b46b-4340-92fe-17da42a78829">Notes</a> – Collaborative writing and sharing of Markdown documents </li>
                <li> <a href="https://helmholtz.cloud/services?search=jupyterlab">Jupyter</a> – Interactive computing via Jupyter Notebooks </li>
                <li> <a href="https://helmholtz.cloud/services?serviceDetails=svc-c5d1516e-ffd2-42ae-b777-e891673fcb32">Helmholtz Codebase</a> – Collaborative software development platform with extensive Continuous Integration (CI) offering </li>
                <li> All Services on <a href="https://helmholtz.cloud/services">Helmholtz Cloud Portal</a> </li>
            </ul>
    </div>
</div>

<div class="image-block">
    <img
        class="help-image left"
        alt="Service Portfolio in terms of ITIL"
        src="{{ site.directory.images | relative_url }}/services/service_portfolio.jpg"
        style="max-width: 20rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <h3>Service Portfolio and Upcoming Services</h3>
        <p>
            Service Portfolio Management in terms of ITIL consists of three main components: 
        </p>
        <ul>
            <li> the <a href="https://hifis.net/doc/service-portfolio/service-portfolio-management/current-services-in-portfolio/  ">Service Pipeline</a> = Upcoming Services </li>
            <li> the Service Catalogue = currently provided Services, available on <a href="https://helmholtz.cloud/services">Helmholtz Cloud Portal</a> </li>
            <li> the Retired Services = taken out of service (currently none) </li>
        </ul>
    </div>
</div>

<div>
    <h3>Seamless Collaboration</h3>
    <p>
        The Helmholtz-wide, internationally accessible authentication and authorization infrastructure <a href="https://aai.helmholtz.de/">Helmholtz AAI</a> enables login to all services with the known credentials of the home Helmholtz centres. Furthermore, also invited guests from outside Helmholtz are able to use Helmholtz Cloud services, e.g. via their home organisation's login, or via public identity providers such as ORCID. In this central login, the Helmholtz AAI, 6500 registrations were recorded as of January 2022, trend rising.
    </p>
    <p>
        The login process from a user's perspective is <a href="https://aai.helmholtz.de/tutorial/2021/06/23/how-to-helmholtz-aai.html">visualized in this tutorial</a>.
    </p>
    <p>
        The organisational framework is provided by the <a href="https://hifis.net/doc/backbone-aai/policies/">Helmholtz AAI Policies</a>.
    </p>
</div>

### Service Operation KPI
The usage of each Helmholtz Cloud Serivce and its overall contribution to the success of the Helmholtz Cloud is monitored continuously.
Please refer to the [documentation pages](https://hifis.net/doc/cloud-service-kpi/) for details on the procedures of KPI data aquisition and calculation,
as well as most recent and previously published results.

<div>
    <h3>Federated scientific community cloud</h3>
    <p>
        Helmholtz Cloud is designed to make the IT services of the service providing Helmholtz Centres available across Helmholtz. Each service is provided by one or more centres, respectively, for the use by other Helmholtz Centres or cross-centre project groups. They also include the possibility of integrating external guests, so that even national and international scientific collaborations are easily possible.
    </p>
</div>

<div>
    <h3>Funded by the Helmholtz Association</h3>
    <p>
        Helmholtz Cloud is funded by the Helmholtz Association of German Research Centres. As a result, the use of Helmholtz Cloud services can be offered free of charge to Helmholtz users and their collaboration partners.
    </p>
</div>

<div>
    <h3>Request for Service and Support</h3>
    <p>
        You cloudn't find what you were looking for? We welcome your suggestions, tips and support. They help us to become better. Please contact us at <a href="mailto:{{site.contact_mail}}">{{site.contact_mail}}</a>.
    </p>
</div>
