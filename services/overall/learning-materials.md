---
title: Learning Materials
title_image: mountains-nature-arrow-guide-66100.jpg
layout: services/default
author: none
additional_css:
    - title/service-title-buttons.css
redirect_from:
    - services/overall/tutorials
    - services/overall/guidelines
    - guidelines
    - workshop-materials
excerpt:
  "We provide learning materials for all purposes related to software engineering and cloud services usage."
---

{{ page.excerpt }}

## Workshop Materials

Our workshop material is available for self-guided learning or as a basis for your own workshops.
Its use and reuse is permitted under a Creative Commons License (see the footers on the workshop pages).

If you find issues or would like to contribute, feel free to take a look at the respective repositories.

### General

| Topic                                  | Materials        | Repository                               |
|----------------------------------------|------------------|------------------------------------------|
| Research Software Publication          | (In preparation) | [Repository][sw-publication-repo]        |
| Container Virtualization in Science    | (In preparation) | [Repository][containers-in-science-repo] |
| Event Management with Indico           | (In preparation) | [Repository][indico-repo]                |
| Getting started with Markdown Flavours | (In preparation) | [Repository][markdown-repo]              |

[containers-in-science-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-using-containers-in-science
[indico-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-event-management-with-indico
[markdown-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-getting-started-with-markdown-flavors
[sw-publication-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/foundations-of-research-software-publication

### Python

| Topic                                | Materials                              | Repository                           |
|--------------------------------------|----------------------------------------|--------------------------------------|
| First Steps in Python                | (In preparation)                       | [Repository][python-first-steps-repo]|
| Object-Oriented Programming          | [Material][python-oop-material]        | [Repository][python-oop-repo]        |
| Data Processing with _Pandas_        | [Material][python-pandas-material]     | [Repository][python-pandas-repo]     |
| Data Visualization with _Matplotlib_ | [Material][python-matplotlib-material] | [Repository][python-matplotlib-repo] |
| Test Automation                      | (In preparation)                       | [Repository][python-test-repo]       |

[python-first-steps-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/first-steps-in-python
[python-matplotlib-material]: {{ "workshop-materials/python-matplotlib/" | relative_url}}
[python-matplotlib-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-matplotlib
[python-oop-material]: {{ "workshop-materials/python-oop/" | relative_url }}
[python-oop-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-oop-in-python
[python-pandas-material]: {{ "workshop-materials/python-pandas/" | relative_url }}
[python-pandas-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-pandas
[python-test-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/test-automation-with-python

### Git and GitLab

| Topic                                   | Materials                      | Repository                       |
|-----------------------------------------|--------------------------------|----------------------------------|
| Introduction to Git                     | (In preparation)               | [Repository][git-intro-repo]     |
| Project Management in GitLab            | (In preparation)               | [Repository][gitlab-basics-repo] |
| GitLab for Software Development in Teams| (In preparation)               | [Repository][gitlab-teams-repo]  |
| Continuous Integration with GitLab CI   | [Material][gitlab-ci-material] | [Repository][gitlab-ci-repo]     |


[git-intro-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/git-introduction
[gitlab-basics-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/workshop-project-management-with-gitlab
[gitlab-ci-material]: {{ "workshop-materials/gitlab-ci/" | relative_url }}
[gitlab-ci-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/gitlab-ci/workshop-materials
[gitlab-teams-repo]: https://gitlab.hzdr.de/hifis/software/education/hifis-workshops/software-development-for-teams

### Further Materials

Not all of our workshop materials are published in a nice fashion (yet).
You can find the complete set of materials in the [Helmholtz Codebase](https://gitlab.hzdr.de/hifis/software/education/hifis-workshops).

### Requesting Workshops

Would you like to have a workshop on a specific topic for your team?

Feel free to <a href="{% link contact.md %}">contact us</a>.

## Tutorials

<div class="flex-cards">
{%- assign posts = site.categories['Tutorial'] | where: "lang", "en" | sort_natural: "title" -%}
{% for post in posts -%}
{% include cards/post_card.html post=post exclude_date=true %}
{% endfor -%}
</div>

## Guidelines

<div class="flex-cards">
{%- assign posts = site.categories['Guidelines'] | where: "lang", "en" | sort_natural: "title" -%}
{% for post in posts -%}
{% include cards/post_card.html post=post exclude_date=true %}
{% endfor -%}
</div>

<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Comments or Suggestions?</h2>
  <p>
    Feel free to <a href="{% link contact.md %}">contact us</a>.
  </p>
</div>
