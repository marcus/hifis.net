---
title: Meet Sam the Scientist!
title_image: blue_curtains.jpg
layout: services/default
additional_css:

excerpt: >-
    No text.
---
## Sam's Story
<figure>
    <img src="{% link assets/img/sam/Sam_hi.png
 %}" alt="person raising their hand" style="float:right;width:7%;min-width:100px;padding:5px 0px 20px 20px;">
</figure>
Hey, I am Sam Scientist and I work at a Helmholtz Centre somewhere in Germany. I'll appear time and again and take you with me into the world of HIFIS. You will hear about the offers that I have had the pleasure of getting to know and use. Be curious, there is a lot of useful stuff in HIFIS.

## How it all begins
Right now I'm really happy as I got the funding confirmation for a new project!
I want to start this soon with some colleagues who work both at another Helmholtz Centre as well as at an external institution.
At lunch, I found a flyer at the cafeteria, describing some of the tools and services HIFIS is offering.
I wonder how HIFIS can help me and what those Helmholtz Federated IT Services are all about.
The claim is to offer **Services for Science — Collaboration made easy**: that sounds promising.

<figure>
    <img src="{% link assets/img/hifisfor/poster-1.svg
 %}" alt="login screen for Helmholtz AAI" style="float:left;width:35%;min-width:250px;padding:5px 20px 20px 5px;">
</figure>
Well, let's take a look at [hifis.net](https://hifis.net) ... 
[HIFIS for Scientists](https://hifis.net/hifisfor/scientists) sounds promising, it describes the life cycle of research.
Tools to set up and plan a project are all offered at the [Helmholtz Cloud Portal](https://helmholtz.cloud/services), let's see what is there.
As it is early on in the project, we need to discuss a lot with everybody involved —
and possibly even would like to easily contact and involve experts from other groups.
The chat service [Mattermost](https://helmholtz.cloud/services?serviceDetails=svc-1be91786-b7e7-4fa3-81d9-1b95dd03cd52) seems like a good idea:
We could also share small documents, pictures, links and everything else that is going to come up during the work. I decide to try it for the communication in the project group.


## So, how does this work? 
<figure>
    <img src="{% link assets/img/sam/Sam-login-aai-cut.png %}" alt="login screen for Helmholtz AAI" style="float:right;width:40%;min-width:250px;padding:5px 0px 20px 20px;">
</figure>
I log in via the [Helmholtz AAI](https://login.helmholtz.de), which is easy, as I only need my institutional account information and 
there is this [tutorial](https://aai.helmholtz.de/tutorial/2021/06/23/how-to-helmholtz-aai.html) to walk me through every step. 
That will be helpful to get the others on board, I guess, so I'll send everyone the link. 

But I wonder how I get those not affiliated with Helmholtz on board? 
I write a mail to the [HIFIS support](mailto:suport@hifis.net) and the answer is really pleasing :
Sometimes the login via the home organization does not work yet, especially for non-Helmholtz partners.
But there are the social providers Google, Orcid or GitHub for this case, so it will be for them as easy as for me, phew!

## Just what we need: Software Development in Teams
As Research Software Engineering will be an important part of our project, 
we need a place to store it, work collaboratively on the code and ensure version control.
I have been working with GitLabs before, so it's really cool to find a version suited for us Helmholtz-based scientists here: 
the [Helmholtz Codebase](https://helmholtz.cloud/services?serviceDetails=svc-c5d1516e-ffd2-42ae-b777-e891673fcb32).
Again, the Helmholtz AAI provides the access and the account is set up automatically right after the log in. 
Awesome, what a relief for our group to have only one login for several services at once.
I spend some time to set up the project as I know already a bit about the software.

But there is an open tab left from when I read the service description: 
[HIFIS Events](https://hifis.net/events), which lists past events and contains a link to the [upcoming courses](https://events.hifis.net/category/4/). 
There, a course covering [Foundations of Research Software Publication](https://events.hifis.net/event/520/) is offered in November.
That's perfect for getting everyone in the project on the same page with this tool!

I'm really excited to have such a good setup for the project within a few minutes. Next time, I'll tell you how we go on from here!

<p></p>
<p style="font-size:13px">Text by Lisa Klaffki, Illustrations by Lorna Schütte.</p>
