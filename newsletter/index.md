---
title: Newsletter
title_image: sign-pen-business-document-48148.jpg
layout: default
additional_css:
    - frontpage.css
    - title/service-title-buttons.css
additional_js: frontpage.js
excerpt:
    HIFIS Newsletter
---

The HIFIS Newsletter will be published quarterly from July 2022 onwards.
Find all published newsletters here in HTML and PDF formats.

Feel free to distribute the links to everyone who might be interested.

## Subscribe and share the links

- Subscribe here to receive all further [**HIFIS newsletter**](https://lists.desy.de/sympa/subscribe/hifis-newsletter) issues automatically.
- For more frequent updates, you are invited to regularly check our [**blog posts page**]({% link posts/index.html %}), and
- Subscribe to <a class="rss-posts-button" href="{{ '/feed/news.xml' | relative_url }}"
                       title="Subscribe to HIFIS Blog Posts RSS Feed (RSS Reader required)"
                       type="application/atom+xml"
                       download>
                       <strong>HIFIS Blog Posts RSS Feed</strong>
                    </a> (RSS Reader required)


Spread the word and share the links. Thank you!

## Published Newsletter Issues so far:

All previous newsletter issues can be found here.

- July 2022: [HTML]({% link newsletter/2022-07/2022-07-HIFIS-Newsletter.html %}), [PDF]({% link newsletter/2022-07/2022-07-HIFIS-Newsletter.pdf %})
- _More to come!_
